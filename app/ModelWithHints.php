<?php
  
  namespace App;

  use Exception;
  use ArrayAccess;
  use JsonSerializable;
  use Illuminate\Support\Arr;
  use Illuminate\Support\Str;
  use Illuminate\Contracts\Support\Jsonable;
  use Illuminate\Contracts\Support\Arrayable;
  use Illuminate\Support\Traits\ForwardsCalls;
  use Illuminate\Contracts\Routing\UrlRoutable;
  use Illuminate\Contracts\Queue\QueueableEntity;
  use Illuminate\Database\Eloquent\Relations\Pivot;
  use Illuminate\Contracts\Queue\QueueableCollection;
  use Illuminate\Support\Collection as BaseCollection;
  use Illuminate\Database\Query\Builder as QueryBuilder;
  use Illuminate\Database\ConnectionResolverInterface as Resolver;
  use Illuminate\Database\Eloquent\Model;

  /**
   * Class ModelWithHints
   * @package App
   *
   * @mixin \Eloquent
   */
  abstract class ModelWithHints extends Model
  {
    //

  }
