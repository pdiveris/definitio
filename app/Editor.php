<?php
  
  namespace App;
  use Illuminate\Database\Eloquent\Model;

  /**
   * App\Editor
   *
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor newModelQuery()
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor newQuery()
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor query()
   * @mixin \Eloquent
   * @property int $id
   * @property string $name
   * @property string $email
   * @property static $handle
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor whereEmail($value)
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor whereId($value)
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor whereName($value)
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor whereHandle($value)
   */
  class Editor extends Model
  {
    //
  }
