<?php
  
  namespace App;
  
  use Illuminate\Database\Eloquent\Model;
  
  /**
 * App\Interaction
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $request_type
 * @property string $referrer
 * @property string $ref_key
 * @property string|null $ip_address
 * @property string|null $meta
 * @property string|null $header
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereMeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereRefKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereReferrer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereRequestType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Interaction whereUpdatedAt($value)
 */
class Interaction extends Model
  {
    //
  }
