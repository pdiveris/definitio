<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Subject
 *
 * @property int $id
 * @property string $subject
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Kalnoy\Nestedset\Collection|\App\Subject[] $children
 * @property-read int|null $children_count
 * @property-read \App\Subject|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subject d()
 */
class Subject extends Model
{
    use NodeTrait;
    
    //
    protected $fillable = ['subject'];
    
}
