<?php
  
  namespace App\Http\Controllers;
  
  use App\Item;
  use App\SiteUser;
  use App\User;
  use Illuminate\Http\Request;
  use Illuminate\Validation\Rules\In;
  
  class DefinitioApplicationManager extends Controller
  {
    private $uploader = 'uppy';          // filepond
    
    public function home(Request $request)
    {
      $alphabet = \App\Alphabet::all();
      return \View::make('manager.home',
        [
        ]
      );
    }
    
    public function getUploader()
    {
      return $this->uploader;
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $params
     * @return \Illuminate\Contracts\View\View
     */
    public function users(Request $request, $params = '')
    {
      if ($params) {
        // linter workaround..
      }
      
      $q = $request->get('q', '');
      
      if (null == $q)
        $q = '';
      
      $letter = $q;
      
      $users = User::where('name', 'like', "$q%")
        ->orderBy('name')
        ->get()
      ;
      
      // $alphabet = \App\Alphabet::all();
      
      $alphabet = \DB::select('CALL `fn_firstletter`(\'name\', \'users\')');
      
      return \View::make('manager.users',
        [
          'alphabet' => $alphabet,
          'users' => $users,
          'letter' => $letter,
          'total' => count($users)
        ]);
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Contracts\View\View
     */
    public function user(Request $request, $id = '')
    {
      $letter = '';
      
      $method = 'PUT';
      if (null == $id || $id == '') {
        $formUser = new SiteUser();
        
        $formUser->email = '';
        $formUser->name = ' ';
        $formUser->handle = '';
        
        $method = 'POST';
      } else {
        $formUser = \App\SiteUser::find($id);
        
        if (null === $formUser) {
          abort(404, "No such user exists. sorry..");
        }
      }
      return \View::make('manager.user',
        [
          'formUser' => $formUser,
          'method' => $method,
          'letter' => $letter,
          'uploader' => $this->getUploader(),
        ]);
    }
    
    public function validateForm(Request $request)
    {
      $validator = \Validator::make($request->all(), [
        'name' => 'required|max:255',
        'email' => 'email|required|max:50',
        'handle' => 'alpha|max:120',
      ]);
      return $validator;
    }
    
    /**
     * Handle update (and possibly insert) requests for single user entities
     *
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function putUser(Request $request, $id = '')
    {
      $user = SiteUser::find($id);
      if (!$user) {
        abort(404);
      }
      $validator = $this->validateForm($request);
      if ($validator->fails()) {
        return redirect('/backstage/user/' . $id)
          ->with('message', 'There are some issues with this entry..')
          ->withErrors($validator->errors())
          ;
      }
      $user->name = $request->get('name');
      $user->email = $request->get('email');
      $user->handle = $request->get('handle');
      $user->enabled = ($request->get('enabled', '') == 'on');
      
      $prefs = [
        'editor' => [
          0 => $request->get('preferencesEditor'),
          1 => $request->get('preferencesEditor') == 'html' ? 'markdown' : 'html'
        ],
        'spelling_checker' => $request->get('preferencesSpellingChecker', '') === 'on' ? true : false
      ];
      
      $user->preferences = $prefs;
      $roleIds = array_values($request->get('roleCheckbox', []));
      
      // catch exception or error
      try {
        $user->save();
        $user->roles()->sync($roleIds);  // it is why we loves laravel
      } catch (\Exception $e) {
        abort(500, $e->getMessage());
      }
      return redirect('/backstage/user/' . $id)
        ->with('message', 'User updated');
    }
    
    /**
     * Handle update (and possibly insert) requests for single user,
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUser(Request $request)
    {
      $id = '';
      $validator = $this->validateForm($request);
      
      if ($validator->fails()) {
        return redirect('/backstage/user/' . $id)
          ->with('message', 'There are some issues with this entry..')
          ->withInput()
          ->withErrors($validator->errors())
          ;
      }
      $user = new SiteUser();
      
      $user->name = $request->get('name');
      $user->email = $request->get('email');
      $user->handle = $request->get('handle');
      $user->enabled = ($request->get('enabled', '') == 'on');
      
      $prefs = [
        'editor' => [
          0 => $request->get('preferencesEditor'),
          1 => $request->get('preferencesEditor') == 'html' ? 'markdown' : 'html'
        ],
        'spelling_checker' => $request->get('preferencesSpellingChecker', '') === 'on' ? true : false
      ];
      
      $user->preferences = $prefs;
      $roleIds = array_values($request->get('roleCheckbox', []));
      
      // catch exception or error
      try {
        $user->save();
        $user->roles()->sync($roleIds);  // it is why we loves laravel
        
      } catch (\Exception $e) {
        abort(500, $e->getMessage());
      }
      
      return redirect('/backstage/user/' . $user->id)
        ->with('message', 'User updated');
      
    }
    
    /**
     * Dispatch request accordingly
     * Dispatcher needed in order to work around the fact that
     * PUT method with Laravel really is a GET request with all the query string consequences..
     *
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function methodDispatcher(Request $request, $id = '')
    {
      if ($id == '') {
        return $this->postUser($request);
      }
      else {
        return $this->putUser($request, $id);
      }
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyUser(Request $request, $id = '')
    {
      $user = \App\SiteUser::find($id);
      if (!$user || $id == '') {
        abort(404);
      }
      
      $user->deleted_at = date("Y-m-d H:i:s");
      $user->save();
      
      return redirect()
        ->back()
        ->with('message', 'User deleted')
        ;
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reviveUser(Request $request, $id = '')
    {
      $user = \App\SiteUser::find($id);
      if (!$user || $id == '') {
        abort(404);
      }
      
      $user->deleted_at = null;
      $user->save();
      
      return redirect()
        ->back()
        ->with('message', 'User brought back from the dead')
        ;
    }
  
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function filePersist(Request $request, $id = '')
    {
      $ret = ['fileops'=>'A town modelled after Chiquitos'];

      if ($request->getHost() == 'definitio.div') {
        dump($request->all());
        dump("id: $id");
      }
      
      //  ||
      if (!is_array( $request->get('data', null) ) ||
        ! array_key_exists('successful', $request->get('data') )
      ) {
        $ret['Error'] = 'Empty request..';
      } else {
        $data = $request['data'];

        foreach ($data['successful'] as $key => $job) {
          $targetName = $job['name']; // e.g. pedro_jisc.jpg
          $targetName = str_replace('@', '-at-', $id) . '.' .$job['extension'];
          
          // dump("targetName: $targetName");
          
          $type = $job['type'];   // e.g. image/jpeg
          $preview = $job['response']['uploadURL'];   // e.g. image/jpeg
          $size = $job['size'];   // e.g. 7343
    
          $allUploaded = ($job['progress']['uploadComplete']) && ($size == $job['progress']['bytesUploaded']);
          // dump("allUploaded $allUploaded");

          
          if ($allUploaded) {
            dump("IN ALL UPLOADED");
            // 2 Read the file
            
            // e.g. https://nus.definitio.org/files/cb5bffde408ddb4d6afa41ba881f29a7
            // this below is for golang tusd
            // $fileURL = $job['uploadURL'];
/*            $client = new \GuzzleHttp\Client();
            // fix for .dev, .div etc environments
            $request = $client->request('GET', $fileURL, [
              'curl' => [
                CURLOPT_SSL_VERIFYPEER => false
              ]
            ]);
            $stream = $request->getBody()->getContents();*/
            
            
            // whereas this is for php-tus
            // https://definitio.div/storage/uploads/5379542306.jpg
            $fileURL = asset('storage/uploads/'.$job['name']);
            
            $filePath = public_path('storage/uploads/'.$job['name']);
            
            $error = false;
            if (false !== ($stream = file_get_contents($filePath))) {
              $error = error_get_last();
            }
            
            // 3. PUT the file
            if (!$error && \Storage::disk('s3')->put($targetName, $stream)) {
              $ret[] = [
                '_loc' => 'Uncharacteristically Pritish',
                'file' => $targetName,
                'status' => 'uploaded',
                'size' => $size,
                'preview' => $preview,
                'type' => $type,
                'store' => 'minio',
                'region' => env('MINIO_REGION'),
                'bucket' => 'avatars',
                'endpoint' => 'https://vault.definitio.org/',
              ];
            }
          }
        }
      }
      // return JSOON response
      return response()->json($ret);
    }
    
  }
