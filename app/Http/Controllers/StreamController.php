<?php
  
  namespace App\Http\Controllers;
  
  use Illuminate\Http\Request;
  use GuzzleHttp\Client;
  
  class StreamController extends Controller
  {
    //
    public function index() {
      // $client = new Client(['base_uri' => "http://rosa:3000"]);
  
      $rootUrl = \Request::getScheme().'://'.\Request::getHost();
  
      $postData = http_build_query([
        'topic' => $rootUrl.'/mercure/demo/books/1.jsonld',
        'data' => json_encode(['key' => 'Pako was here']),
      ]);

      $ctx = stream_context_create([
          'http' => [
            'method'  => 'POST',
            'header'  => "Content-type: application/x-www-form-urlencoded\r\nAuthorization: Bearer ".env('MERCURE_JWT_SECRET'),
            'content' => $postData
          ],
          'ssl' => [
            "verify_peer" => false,
            "verify_peer_name" => false,
          ]
        ]
      );
      
      dump($ctx);
     
      echo file_get_contents($rootUrl.'/mercure/hub', false, $ctx);
      
    }
    
    public function client() {
      $rootUrl = \Request::getScheme().'://'.\Request::getHost();
      
      return \View::make('stream.client',
        [
          'rootUrl'=>$rootUrl,
        ]
      );
    }
    
    public function tristram() {
      $rootUrl = \Request::getScheme().'://'.\Request::getHost();
      return \View::make('stream.tristram',
        [
          'tristram'=>true,
          'rootUrl'=>$rootUrl,
        ]
      );
      
    }
  }
