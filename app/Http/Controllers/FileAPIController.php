<?php

namespace App\Http\Controllers;

use Bentleysoft\Events\Observer;
use Bentleysoft\FileNavigator\Audit;
use Bentleysoft\FileNavigator\DirectoryResource;
use Bentleysoft\FileNavigator\FileResource;
use Bentleysoft\FileNavigator\Security;
use Bentleysoft\Finder\FinderElement;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Integer;

class FileAPIController extends Controller
{
    /**
     * Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
     *
     * @var string
     */
    private static $disk = 'public';
    
    /**
     * @var string
     */
    private static $cwd = '';
    
    public function __construct()
    {
    }
    
    /**
     * Set the working disk
     *
     * @param string $disk
     * @return bool
     */
    public static function setDisk(string $disk): bool
    {
        self::$disk = $disk;
        return true;
    }
    
    public static function pwd(): string
    {
        return self::$cwd;
    }
    
    public static function cd(string $path): bool
    {
        self::$cwd = $path;
        return true;
    }
    
    /**
     * @param array $list
     * @return array
     */
    public static function makeStruct(array $list, $disk)
    {
        $ret = [];
        foreach ($list as $i => $rs) {
            $ret[] = FinderElement::createFromPath($rs, $disk);
        }
        return $ret;
    }
    
    /**
     * implementation og unix ls
     *
     * @param string $pattern
     * @param array $params
     * @param string $disk
     * @return array
     */
    public static function list(string $pattern, array $params, string $disk)
    {
        $resources =  \Storage::disk($disk)->listContents($pattern, false);
        
        usort($resources, function ($a, $b) {
            return strcmp(strtolower($b['filename']), strtolower($a['filename']));
        });
        
        usort($resources, function ($a, $b) {
            return ($a['type'] === 'dir') ? -1 : 1;
        });
        
        return $resources;
    }
    
    public static function copy($pattern, $destination, $params)
    {
    }
    
    public static function remove($pattern, $params)
    {
    }
    
    public static function move($source, $target, $params)
    {
    }
    
    public function bak()
    {
        /*
        $root = new DirectoryResource('assets');
        $dir = new Observer('DirectoryObserver', 50);
        $root->attach($dir);
        $dir = new Observer('DirectoryObserver', 50);
        $file = new Observer('FileObserver', 20);
        $fs = new Observer('FileSystem',10);

        $root->attach($fs);
        $root->attach($file);
        $root->attach($dir);
        $ret['assets'] = $root;
        $root->setEvent(sprintf("Contents of resource read (%s)", $root->getType()));

        foreach ($resources as $resource) {
            $node = null;
            if ($resource['type']==='dir') {
                $node = new DirectoryResource($resource['basename']);
            } elseif ($resource['type']==='file') {
                $node = new FileResource($resource['basename'], $resource['size']);
                // $stream = \Storage::disk('s3')->get($resource['dirname'].'/'.$resource['basename']);
            }
            $ret[$resource['dirname']]->addResource($node);
        }
        */
    }
}
