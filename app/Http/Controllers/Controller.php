<?php
  
  namespace App\Http\Controllers;

  use Illuminate\Foundation\Bus\DispatchesJobs;
  use Illuminate\Routing\Controller as BaseController;
  use Illuminate\Foundation\Validation\ValidatesRequests;
  use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
  
  class Controller extends BaseController
  {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
  
    /**
     * A "facade" or proxy.
     * It will come in useful as some stage.
     *
     * @param $request
     * @return \App\Http\Controllers\Controller
     *
     * @author pdiveris
     */
    public static function fascia() {
      $fascia = new static();
      return $fascia;
    }
    
  }
