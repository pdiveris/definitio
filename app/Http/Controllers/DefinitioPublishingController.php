<?php
  
  namespace App\Http\Controllers;
  
  use Barryvdh\Snappy\Facades\SnappyPdf;
  use Dompdf\Dompdf;
  use Dompdf\Options;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Redis;
  
  class DefinitioPublishingController extends Controller
  {
    //
    public function __construct()
    {
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Contracts\View\View
     *
     * Step1/N
     * e.g. https://definitio.div/publish/term/69
     */
    public function publish(Request $request, $id = '')
    {
      if (is_numeric($id)) {
        $terms = \App\Item::where('id', '=', $id)
          ->where('deleted_at', '=', null)
          ->orWhere('deleted_at', '=', '')
          ->get()
        ;
      }
      else {
        $terms = \App\Item::where('slug', '=', $id)
          ->where('deleted_at', '=', null)
          ->orWhere('deleted_at', '=', '')
          ->get()
        ;
      }
      
      $term = $terms[0];
      $term->content_html = self::makeDropCaps($term->content_html);
      
      
      if (count($terms) < 1) {
        abort(404);
      }
      return \View::make('booklet', ['term' => $term]);
    }
    
    /**
     * Generate a PDF with dompdf
     * @see https://dompdf.net/index.php
     *
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function pdfDompdf(Request $request, $id = '')
    {
      $url = $request->getUri();
      // https://definitio.div/publish/term/absalom
      // https://definitio.div/pdf/term/absalom
      
      $ret = false;
      
      $client = new \GuzzleHttp\Client();
  
      $url = str_replace('/pdf/', '/publish/', $url);
      $request = $client->request('GET', $url, [
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false
        ]
      ]);
      
      $arrContextOptions = array(
        "ssl" => array(
          "verify_peer" => false,
          "verify_peer_name" => false,
        ),
      );
      
      $stream = $request->getBody()->getContents();
      
      $file_name = htmlentities($id);
      
      file_put_contents(storage_path("artifacts/dompdf/$file_name.html"), $stream);
      $filePath = storage_path("artifacts/dompdf/$file_name.pdf");
      
      if (env('DOMPDF_CACHE', false) && file_exists($filePath)) {
        $fileTime = new \DateTime(date('Y-m-d H:i:s', filemtime($filePath)));
        
        $now = now();
        $stream = file_get_contents($filePath);
        
        //
        if ($now->diff($fileTime)->s <= env('DOMPDF_CACHE_TTL', 3600)) {
          $ret = new \Illuminate\Http\Response($stream, 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $id . '.pdf' . '"',
          ));
        }
      }
      else {
        if (!env('DOMPDF_FACADE', false)) {
          $pdf = new Dompdf();
          
          $options = new Options([
              'dpi' => 150,
              'defaultFont' => 'sans-serif',
              'isRemoteEnabled' => true,
              'isHtml5ParserEnabled' => true,
              'isFontSubsettingEnabled' => false,
              "font_dir" => storage_path('fonts/'),
              "font_cache" => storage_path('fonts/'),
              "temp_dir" => sys_get_temp_dir(),
              "chroot" => realpath(base_path()),
              "enable_font_subsetting" => false,
              "pdf_backend" => "CPDF",
              "enable_javascript" => true,
              "enable_html5_parser" => true,
            ]
          );
          
          $pdf->setOptions($options);
          $pdf->setHttpContext(stream_context_create($arrContextOptions));
          
          $pdf->loadHtml($stream);
          $pdf->render();
          
          file_put_contents(storage_path("artifacts/dompdf/$file_name.pdf"), $pdf->output(), 0);
          $ret = new \Illuminate\Http\Response($pdf->output(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $id . '.pdf' . '"',
          ));
        }
        else {
          $ret = \PDF::setHttpContext($arrContextOptions)
            ->loadHTML($stream)
            ->save(storage_path("artifacts/dompdf/$file_name.pdf"))
            ->stream($id . '.pdf')
          ;
        }
      }
      return $ret;
    }
    
    public function pdfWebkit($request, $id = '')
    {

      if (is_object($request) && method_exists($request, 'getUri') ) {
        $url = $request->getUri();
      } else {
        // https://definitio.div/term/john-a-study-in-late-psychopathy.pdf
        // https://definitio.div/publish/term/john-a-study-in-late-psychopathy
        $url = $request;
      }
      
      $ret = false;
      $file_name = htmlentities($id);
  
      $url = str_replace('/pdf/', '/publish/', $url);
 
      $client = new \GuzzleHttp\Client();
      
      $request = $client->request('GET', $url, [
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false
        ]
      ]);
  
      $stream = $request->getBody()->getContents();
      
      $filePath = storage_path("artifacts/dompdf/$file_name.pdf");
  
      if (env('DOMPDF_CACHE', false) && file_exists($filePath)) {
        $fileTime = new \DateTime(date('Y-m-d H:i:s', filemtime($filePath)));
    
        $now = now();
        $stream = file_get_contents($filePath);
        
        //
        if ($now->diff($fileTime)->s <= env('DOMPDF_CACHE_TTL', 3600)) {
          $ret = new \Illuminate\Http\Response($stream, 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $id . '.pdf' . '"',
          ));
        } else {

          $snappy = SnappyPdf::loadHTML($stream);
          file_put_contents(storage_path("artifacts/dompdf/$file_name.pdf"), $snappy->output(), 0);
  
          $ret = new \Illuminate\Http\Response($snappy->output(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $id . '.pdf' . '"',
          ));
          
        }
      } else {
        $snappy = SnappyPdf::loadHTML($stream);
        file_put_contents(storage_path("artifacts/dompdf/$file_name.pdf"), $snappy->output(), 0);
 
        $ret = new \Illuminate\Http\Response($snappy->output(), 200, array(
          'Content-Type' => 'application/pdf',
          'Content-Disposition' => 'inline; filename="' . $id . '.pdf' . '"',
        ));
        
      }
      return $ret;
      // return $snappy->inline();

    }
  
    /**
     * Render a PDF with dompdf
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function pdf(Request $request)
    {
      return \PDF::loadFile(public_path() . '/pdf.html')
        ->save(storage_path('artifacts/dompdf/') . 'file.pdf')
        ->stream('download.pdf')
        ;
    }
    
    public function pdfMpdf(Request $request, $id = '')
    {
      $file_name = htmlentities($id);
      
      $stream = file_get_contents(storage_path("artifacts/dompdf/$file_name.html"));
      
      $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
      $fontDirs = $defaultConfig['fontDir'];
      
      $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
      $fontData = $defaultFontConfig['fontdata'];
      
      $options = ['fontDir' => array_merge($fontDirs, [
        public_path('/fonts'),
      ]),
        'fontdata' => $fontData + [
            'avant' => [
              'R' => 'avant.ttf',
            ],
            'didot' => [
              'R' => 'GFSDidot.ttf',
              'I' => 'GFSDidotItalic.ttf',
            ],
          
          ],
        'default_font' => 'avant',
        'curlAllowUnsafeSslRequests' => true,
      ];
      
      $mpdf = new \Mpdf\Mpdf($options);
      
      $mpdf->WriteHTML($stream);
      
      $mpdf->Output();
      
      return $stream;
    }
    
    /**
     * Transform model elements attributes and/or text attributes
     *
     * @see https://ckeditor.com/docs/ckeditor5/latest/api/module_engine_conversion_conversion-Conversion.html#function-attributeToAttribute
     * @see https://www.phpliveregex.com/#tab-preg-replace
     * @see http://php.net/manual/en/function.preg-replace.php
     *
     * @param string $input
     * @return string
     */
    public static function makeDropCaps(string $input = ''): string
    {
      /*

      preg_replace('/(<i[^>]*>)([\s\S]+)(<\/i>)/', '$0 --> $2', $input_lines);
      preg_replace('/(.*)(<i class="dropcap">)([\s\S]+)(<\/i>)(.*)/', '', $input_lines);
  
      find:
      <i class="dropcap">J</strong></i>
 
      replace:
      <div class="outercap"><div class="innercap">W</div></div>
      */
      
      // $ret = '<i class="dropcap">J</i>';
      $ret = $input;
      
      $replace = '$1<div class="outercap"><div class="innercap">$3</div></div>$5';
      $ret = preg_replace('/(.*)(<i class="dropcap">)([a-zA-Z])(<\/i>)/', $replace, $ret);
      
      return $ret;
    }
    
  }
