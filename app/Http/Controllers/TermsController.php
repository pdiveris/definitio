<?php

namespace App\Http\Controllers;

use App\Console\Commands\strip_tags;
use App\Item;
use App\Term;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class TermsController
 *
 * @package App\Http\Controllers
 */
class TermsController extends Controller
{
    private $request;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Display a listing of the resource.
     * With pagination and filtering
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $q = $request->get('q', '');
    
        if (null === $q) {
            $q = '';
        }
    
        $letter = $q;
    
        $terms = \App\Item::where('expression', 'like', "$q%")
            ->orderBy('expression')
            ->forPage(1, 22)
            ->get()
        ;
    
        $alphabet = \App\Alphabet::where('initial', '>=', '')->get();
    
        return \View::make(
            'cms.terms',
            [
                'alphabet' => $alphabet,
                'letter' => $letter,
                'terms' => $terms,
                'total' => count($terms)
            ]
        );
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     * @throws \Exception
     */
    public function create()
    {
        $term = new \App\Item();
    
        $labels = [];
        preg_match('/(?<=\[).+?(?=\])/', $term->src, $labels, PREG_OFFSET_CAPTURE);
    
        $term->content_html = self::prepareText($term->content_html);
    
        $src = [];
        preg_match('/(?<=\().+?(?=\))/', $term->src, $src, PREG_OFFSET_CAPTURE);
    
        $source = ['label' => [''], 'href' => ['']];
        if (count($labels) > 0) {
            $source['label'] = $labels[0];
        }
    
        if (count($src) > 0) {
            $source['href'] = $src[0];
        }
    
        //$term->published_at = '1973-11-17';
        if (!(null === $term->published_at)) {
            $term->published_at = Date('Y-m-d H:i:s', strtotime($term->published_at));
        }
    
        $related = [];
        foreach ($term->related as $rel) {
            $related[] = $rel->expression;
        }
        
        // Multiple values
        Inertia::share([
            // Synchronously
            'app' => [
                'name' => \Config::get('app.name')
            ],
            // Lazily
            'auth' => function () {
                return [
                    'user' => \Auth::user() ? [
                        'id' => \Auth::user()->id,
                        'name' => \Auth::user()->name,
                        'email' => \Auth::user()->email,
                    ] : null
                ];
            }
        ]);
    
        return Inertia::render('Term/Create')
            ->with('term', $term);
    }

    public function lola(Request $request)
    {
        return Inertia::render('Dashboard/Lola')
            ->with('term', []);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::User();
        $preview = $request->get('preview', false);

        if ((!(int)$user->id) > 0) {
            abort(403);
        }
    
        $validator = \Validator::make($request->all(), [
            'expression' => 'required|max:2550',
        ]);
    
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors())
                ;
        }
    
        $term = new \App\Term();
        $term->expression = $request->input('expression');
    
        $term->content_html = $request->input('content_html');
        $term->body = strip_tags($term->content_html);
    
        $term->published_at = $request->input('published_at');
    
        if ($request->input('link_href') !== '') {
            $term->src = '[' . $request->input('link_label') . '](' . $request->input('link_href') . ')';
        }
    
        // handle versioning!
        $term->user_id = $user->id ?? 0;
    
        $term->save();
        
        $item = \App\Item::find($term->id);
        if (!$item) {
            abort(502, 'Something is not right here..');
        }
    
        $url = '/term/'.$item->slug;
        $headers = array();
        $headers[] = ['x-inertia'=>true];

        if ($preview) {
            $request->session()->flash('success', 'Term created!');
            return response(['success'=>'Yeah'], SymfonyResponse::HTTP_CONFLICT)
                ->header('x-inertia-location', $url);
        }
    
        return \Response::redirectTo('/terms/edit/' . $term->id, 303, $headers)
            ->with('success', 'Term updated!')
        ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param null $id
     * @return \Inertia\Response
     * @throws \Exception
     *
     * @see https://stackoverflow.com/questions/41924476/vuejs-v-for-add-bootstrap-row-every-5-items
     */
    public function edit($id = null)
    {
        if (null === $id || $id === '') {
            $term = new \App\Item();
        } else {
            $term = \App\Item::find($id);
            // throw exception if not
        }
        
        $labels = [];
        preg_match('/(?<=\[).+?(?=\])/', $term->src, $labels, PREG_OFFSET_CAPTURE);
    
        $term->content_html = self::prepareText($term->content_html);
    
        $src = [];
        preg_match('/(?<=\().+?(?=\))/', $term->src, $src, PREG_OFFSET_CAPTURE);
    
        $source = ['label' => [''], 'href' => ['']];
        if (count($labels) > 0) {
            $source['label'] = $labels[0];
        }
    
        if (count($src) > 0) {
            $source['href'] = $src[0];
        }
    
        //$term->published_at = '1973-11-17';
        if (!null === $term->published_at) {
            $term->published_at = Date('Y-m-d H:i:s', strtotime($term->published_at));
        }
    
        $related = [];
        foreach ($term->related as $rel) {
            $related[] = $rel->expression;
        }
    
        // Multiple values
        Inertia::share([
            // Synchronously
            'app' => [
                'name' => \Config::get('app.name')
            ],
            // Lazily
            'auth' => function () {
                return [
                    'user' => \Auth::user() ? [
                        'id' => \Auth::user()->id,
                        'name' => \Auth::user()->name,
                        'email' => \Auth::user()->email,
                    ] : null
                ];
            }
        ]);
        
        return Inertia::render('Term/Edit')
            ->with('term', $term);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $term = \App\Item::find((int)$id);
        $preview = $request->get('preview', false);
        
        if ($term !== null && $term->id > 0) {
            $term->expression = $request->get('expression');
            $term->content_html = $request->get('content_html');
            $term->body = strip_tags($term->content_html);
            $term->save();
        }

        if ($preview) {
            $url = '/term/'.rawurldecode($term->slug);

            // dump('Redirecting to: '.$term->slug);
            
            $request->session()->flash('success', 'Term updated!');
            return response(['success'=>'Success'], SymfonyResponse::HTTP_CONFLICT)
                ->header('x-inertia-location', $url);
        }

        $headers = array();
        $headers[] = ['x-inertia'=>true];

        return \Response::redirectTo('/terms/edit/' . $id, 303, $headers)
            ->with('success', 'Term updated!')
            ;
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, $id)
    {
        $permanent = $request->get('permanent', 'false') === 'true';
        
        $term = Term::find($id);
        if (!$term || $id == '') {
            abort(404);
        }
    
        if (!$permanent) {
            $term->deleted_at = date("Y-m-d H:i:s");
            $term->save();
        } else {
            $term->delete();
            $request->session()->flash('message', 'Term deleted');
            return response(['success'=>'Yeah'], SymfonyResponse::HTTP_CONFLICT)
                ->header('x-inertia-location', '/terms');
        }
        return redirect()
            ->back()
            ->with('message', 'Term deleted')
            ;
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function revive($id = '')
    {
        $term = Term::find($id);
        if (!$term || $id === '') {
            abort(404);
        }
        
        $term->deleted_at = null;
        $term->save();
        
        return redirect()
            ->back()
            ->with('message', 'Term brought back from the dead')
            ;
    }
    
    
    /**
     * @param string $src
     * @return mixed|string
     * @throws \Exception
     */
    public static function prepareText($src = '')
    {
        $ret = $src;
        
        // convert <label class="label-red">Expand...</label>
        $ret = str_replace(
            "@expand",
            '<label class="label-red">Expand...</label>',
            $ret
        );
        
        return $ret;
    }
}
