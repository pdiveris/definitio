<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Inertia\Inertia;
use App\InertiaPlus\InertiaPlus;
use App\Http\Controllers\FileAPIController;
use Inertia\Inertia;

class FileManagerController extends Controller
{
    private $request;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * @return \App\InertiaPlus\ResponsePlus
     * @throws \Exception
     */
    public function index()
    {
        $dir = $this->request->get('dir', 'assets');
        $disk = $this->request->get('disk', 's3');
        
        $modal = ! ($this->request->get('modal', '') === 'open');
        
        // Multiple values
        InertiaPlus::share([
            // Synchronously
            'app' => [
                'name' => \Config::get('app.name')
            ],
            // Lazily
            'auth' => function () {
                return [
                    'user' => \Auth::user() ? [
                        'id' => \Auth::user()->id,
                        'name' => \Auth::user()->name,
                        'email' => \Auth::user()->email,
                    ] : null
                ];
            }
        ]);
        
        $pat = $dir;   // that's because we may be adding * and other wildcards, OK?
        
        $resources = FileAPIController::list($pat, [], $disk);
        $struct = FileAPIController::makeStruct($resources, $disk);

        $resp = InertiaPlus::render('Fileman/Index', [
            'resources' => $resources,
            'struct' => $struct,
        ], $modal);
        
        return $resp;
    }
    
    /**
     * Fetch from S3 etc..
     *
     * @param $path
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException*@throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function fetchFromBucket($disk, $path): string
    {
        return \Storage::disk($disk)->get($path);
    }
    
    /**
     * // /preview/s3/assets/photos/DSC_0034.jpg
     * @param $path
     * @return
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function preview($disk, $path)
    {
        switch ($disk) {
            case 's3':
                $stream = self::fetchFromBucket($disk, $path);
                return response($stream, 200)->header("Content-Type", 'image/jpeg');
        }
    }
    
    public function upload()
    {
        return view('cms.upload');
    }
}
