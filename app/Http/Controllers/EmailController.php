<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmailController extends Controller
{
    //
  public function send(Request $request){
    //Logic will go here
    $title = $request->input('title');
    $content = $request->input('content');
  
    \Mail::send('emails.welcome', ['title' => $title, 'content' => $content], function ($message)
    {
    
      $message->from('petros@definitio.org', 'Petros Diveris');
    
      $message->to('petros@diveris.org');
    
    });
  
  
    return response()->json(['message' => 'Request completed']);
  }
  
  public function getSend(Request $request, $title, $content){
    //Logic will go here
    
    \Mail::send('emails.welcome', ['title' => $title, 'content' => $content], function ($message)
    {
      
      $message->from('petros@definitio.org', 'Petros Diveris');
      
      $message->to('petros@diveris.org');
      
    });
    
    
    return response()->json(['message' => 'Request completed']);
  }
}
