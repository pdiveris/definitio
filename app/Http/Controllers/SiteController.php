<?php
    
    namespace App\Http\Controllers;
    
    use App\Helpers\Utils;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Input;
    use App\Item;
    use App\Alphabet;
    use Inertia\Inertia;
    
    /**
     * Class SiteController
     * @package App\Http\Controllers
     */
class SiteController extends Controller
{
    private $request;
        
    private static $meta = [
        'og' => [
            'og:url' => '',
            'og:type' => '',
            'og:title' => '',
            'og:description' => '',
            'og:image' => '',
            'og:image:type' => 'image/png',
            'og:image:width' => '1024',
            'og:image:height' => '768',
        ],
        'article' => [
            'article:author' => '',
        ],
        'fb' => [
            'fb:app_id' => '',
        ],
    ];
    
    private static $staticContent = [
        'privacy',
        'aboutus',
        'about-us',
        'contactus',
        'contact-us',
    ];
        
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
        
    public function readFileSubDir($scanDir)
    {
        $handle = opendir($scanDir);
        while (($fileItem = readdir($handle)) !== false) {
            // skip '.' and '..'
            if (($fileItem == '.') || ($fileItem == '..')) {
                continue;
            }
            $fileItem = rtrim($scanDir, '/') . '/' . $fileItem;
            // if dir found call again recursively
            if (is_dir($fileItem)) {
                foreach ($this->readFileSubDir($fileItem) as $childFileItem) {
                    yield $childFileItem;
                }
            } else {
                yield $fileItem;
            }
        }
        closedir($handle);
    }
        
    public function dir()
    {
        foreach ($this->readFileSubDir('/Users/pedro/PhpstormProjects/definitio2/app') as $fileItem) {
            echo($fileItem . "<br/>\n");
        }
    }
        
    /**
     * @return mixed
     */
    public function home()
    {
        $q = $this->request->get('q', '');
        $q = $q ?? '';
        $letter = $q;
            
        $items = Item::where('expression', 'like', "$q%")
            ->where(function ($query) {
                $query->where('deleted_at', '=', '')
                    ->orWhereNull('deleted_at');
            })
            ->orderBy('expression')
            ->get();
            
        $alphabet = Alphabet::where('initial', '>=', '')
                                ->where(function ($query) {
                                    $query->where('deleted_at', '=', '')
                                        ->orWhereNull('deleted_at');
                                })
                                ->get();
        
        return view('home', [
                'alphabet' => $alphabet,
                'items' => $items,
                'q' => $q,
                'letter' => $letter
            ]);
    }

    /**
     * Do some text transformations
     * For instance, apply dropcaps
     *
     * @param string $src
     * @return string
     */
    public static function applyTextTransformations($src = '')
    {
        $ret = $src;

        $ret = DefinitioPublishingController::makeDropCaps($ret);
        $ret = DefinitioPublishingController::makeBulmaHeadings($ret);

        return $ret;
    }
        
    /**
     * Add the Expand/collapse toggler
     *
     * @param string $s
     * @return string
     */
    public static function addExpands(string $s)
    {
        $ret = str_replace(
            '@expand',
            '<a class="toggler">Read more +</a><div class="expander is-hidden"',
            $s
        );
        return $ret;
    }
        
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $slug
     * @return \Illuminate\Contracts\View\View | string
     * @throws \Exception
     */
    public function term(Request $request, $slug = '')
    {
        $letter = '';
        $q = Input::get('q', '');
            
        $format = Input::get('format', 'html');
            
        // return file if it's a media format..
        // if (Utils::isMedia($format)) {
        if (Utils::isMedia($format)) {
            return response()->file(MediaController::takeSaveAndReturnSnapFromUrl($request->getUri()));
        }
            
        if ($q !== '' && $slug === '') {
            $slug = $q;
        }
            
        if ('' !== $slug) {
            $items = \App\Item::where('slug', '=', $slug)
                ->where('deleted_at', '=', null)
                ->orWhere('deleted_at', '=', '')
                ->get()
            ;
        } else {
            $items = \App\Item::inRandomOrder()
                ->where('deleted_at', '=', null)
                ->orWhere('deleted_at', '=', '')
                ->get()
            ;
        }
            
        if (count($items) > 0) {
            $letter = substr($items[0]->expression, 0, 1);
            $item = $items->first();
        } else {
            abort(404);
        }
            
        $alphabet = \App\Alphabet::all();

        $Parsedown = new \ParsedownExtra();
        $srcMarkup = ($item->src !== null && $item->src !== '[]()') ? $Parsedown->text($item->src) : '';
            
        $srcMarkup = str_replace('<p>', '<p><em>Source: </em>', $srcMarkup);
            
        $item->content_html = self::addExpands($item->content_html);
        $item->content_html = self::applyTextTransformations($item->content_html);
            
        DefinitioPublishingController::setMeta(
            'og',
            [
                'url' => $request->getUri(),
                'description' => htmlentities($item->expression) . '',
                'title' => $item->expression,
                'type' => 'article',
                'site_name' => env('APP_NAME

                ', ''),
                'image' => $request->getUri() . '.png',
                //'image' => 'https://www.definitio.org/assets/snaps/pan-europa.png',
            ]
        );
            
        DefinitioPublishingController::setMeta('article', ['author' => $item->user->canoniseName(),]);

        return \View::make(
            'term',
            [
                'alphabet' => $alphabet,
                'term' => $item,
                'q' => $q,
                'letter' => $letter,
                'srcMarkup' => $srcMarkup
            ]
        );
    }
        
        
    /**
     * @return string
     */
    public function catchAll()
    {
        $slug = \Request::route()->uri;
        // get rid of - in about-us
        if (in_array($slug, self::$staticContent)) {
            $slug = str_replace('-', '', $slug);
        } else {
            abort(404);
        }
    
        $q = '';
        $letter = $q;
        $alphabet = \App\Alphabet::where('initial', '>=', '')->get();
    
        return \View::make('static.'.$slug, ['alphabet' => $alphabet, 'letter' => $letter]);
    }
        
    /**
     * "Static" content such as Privacy, About Us, T&Cs
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function getShowContentStatic()
    {
        $slug = \Request::route()->uri;
        // get rid of - in about-us
        if (in_array($slug, self::$staticContent)) {
            $slug = str_replace('-', '', $slug);
        } else {
            abort(404);
        }

        $q = '';
        $letter = $q;
        $alphabet = \App\Alphabet::where('initial', '>=', '')->get();

        return \View::make('static.'.$slug, ['alphabet' => $alphabet, 'letter' => $letter]);
    }
    
    
    /**
     * Contact form in various modes
     * content/forms
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function contact()
    {
        return \View::make(
            'contact',
            [
                'request' => \Request::route()->uri
            ]
        );
    }
}
