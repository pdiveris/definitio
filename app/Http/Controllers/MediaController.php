<?php
  
  namespace App\Http\Controllers;
  
  class MediaController extends Controller
  {
    /**
     * @param string $url
     * @param string $fileName
     * @return string
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public static function takeSaveAndReturnSnapFromUrl($url = '', $fileName = '') {
      if (strpos($url, '?') !== false ) {
        $url = substr($url, 0, strpos($url, '?'));
      }
      
      if ($url === '') {
        $url = 'https://www.definitio.org/term/trickle-down';
      }
      
      if ($fileName === '') {
        // e.g. $filePath = public_path('assets/snaps/snap.png');
        $bits = explode('/', $url);
        if (count($bits) > 0) {
          $fileName = $bits[count($bits)-1];
          $fileName = $fileName . '.png';
        }
      }
      $filePath = public_path('assets/snaps/'.$fileName);
      
      if (!file_exists($filePath)) {
        $snappy =  \SnappyImage::setOption('width','1480')
          ->setOption('height','1112')
          ->setOption('run-script','javascript: snapper();')
          ->setOption('format','png')
          // thiw below doesn't pass "snapper" orwhatever else, however that's the only way to pass wkid itself!
          ->setOption('custom-header', ['wkid'=>"snapper"] )
          ->loadFile($url);
        
        file_put_contents($filePath, $snappy->output());
      }
      return $filePath;
    }
    
    /**
     * Turn into a generic snapshooter
     *
     * @param string $url
     * @return string
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public static function urlScreenShot($url = '') {
      if ($url === '') {
        $url = 'https://www.definitio.org/term?q=Trickle%20down';
      }
      
    }
    
    
  }
