<?php
  
  namespace App\Http\Controllers;
  
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Input;
  use ParsedownExtra;
  use App\Helpers\Utils;
  
  /**
   * Class DefinitioSiteController
   * @package App\Http\Controllers
   */
  class DefinitioSiteController extends Controller
  {
    private static $meta = [
      'og' => [
        "og:url" => '',
        "og:type" => '',
        "og:title" => '',
        "og:description" => '',
        "og:image" => '',
        "og:image:type" => 'image/png',
        "og:image:width" => '1024',
        "og:image:height" => '768',
      ],
      'article' => [
        'article:author' => '',
      ],
      'article' => [
        'article:author' => '',
      ],
      'fb' => [
        'fb:app_id' => '',
      ],
    ];
    
    private static $staticContent = [
      'privacy',
      'aboutus',
      'about-us',
      'contactus',
      'contact-us',
    ];
    
    /**
     * DefinitioSiteController constructor.
     */
    public function __construct()
    {
      //
    }
    
    /**
     * @param string $type
     * @return array|mixed
     */
    public static function getMeta($type = '')
    {
      return $type === '' ? self::$meta : self::$type;
    }
    
    /**
     * @return string
     */
    public static function getMetaMarkup()
    {
      $ret = '';
      if (null !== self::getMeta()) {
        foreach (self::getMeta() as $type => $values) {
          foreach ($values as $property => $content) {
            $ret .= "<meta property=\"$property\" content=\"$content\"/>\n";
          }
        }
      }
      return $ret;
    }
    
    /**
     * @param string $type
     * @param array $values
     */
    public function setMeta($type = 'og', $values = [])
    {
      foreach ($values as $key => $value) {
        if ($type !== 'standalone') {
          $key = $type . ':' . $key;
        } else {
          $key = '' . $key;;
        }
        if (array_key_exists($type, self::$meta)) {
          self::$meta[$type][$key] = $value;
        }
      }
    }
    
    //
    
    /**
     * @return \Illuminate\Contracts\View\View
     *
     * @see https://intranet.birmingham.ac.uk/as/libraryservices/library/referencing/icite/referencing/harvard/referencelist.aspx
     *
     */
    public function alphabet()
    {
      $q = Input::get('q', '');
      
      if (null == $q)
        $q = '';
      
      $letter = $q;
      
      $items = \App\Item::where('expression', 'like', "$q%")
        ->where('deleted_at', '=', null)
        ->orWhere('deleted_at', '=', '')
        ->orderBy('expression')
        ->get()
      ;
      
      $alphabet = \App\Alphabet::where('initial', '>=', '')->get();
      
      return \View::make('home', ['alphabet' => $alphabet, 'items' => $items, 'q' => $q, 'letter' => $letter]);
    }
    
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function dropCap()
    {
      return \View::make('dropcaps', []);
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $slug
     * @return \Illuminate\Contracts\View\View | string
     * @throws \Exception
     */
    public function term(Request $request, $slug = '')
    {
      $letter = '';
      $q = Input::get('q', '');
      
      $format = Input::get('format', 'html');
      
      // return file if it's a media format..
      // if (Utils::isMedia($format)) {
      if (Utils::isMedia($format)) {
        return response()->file(MediaController::takeSaveAndReturnSnapFromUrl($request->getUri()));
      }
      
      if ($q !== '' && $slug === '') {
        $slug = $q;
      }
      
      if ('' !== $slug) {
        $items = \App\Item::where('slug', '=', $slug)
          ->where('deleted_at', '=', null)
          ->orWhere('deleted_at', '=', '')
          ->get()
        ;
      } else {
        $items = \App\Item::inRandomOrder()
          ->where('deleted_at', '=', null)
          ->orWhere('deleted_at', '=', '')
          ->get()
        ;
      }

      if (count($items) > 0) {
        $letter = substr($items[0]->expression, 0, 1);
      }
      else {
        abort(404);
      }
      
      $alphabet = \App\Alphabet::all();
      
      $Parsedown = new ParsedownExtra();
      $srcMarkup = ($items[0]->src !== null && $items[0]->src !== '[]()') ? $Parsedown->text($items[0]->src) : '';
      
      $srcMarkup = str_replace('<p>', '<p><em>Source: </em>', $srcMarkup);
      

      $items[0]->content_html = self::addExpands($items[0]->content_html);
      $items[0]->content_html = self::applyTextTransformations($items[0]->content_html);
      
      self::setMeta(
        'og',
        [
          'url' => $request->getUri(),
          'description' => htmlentities($items[0]->expression) . '',
          'title' => $items[0]->expression,
          'type' => 'article',
          'site_name' => env('APP_NAME', ''),
          'image' => $request->getUri() . '.png',
          //'image' => 'https://www.definitio.org/assets/snaps/pan-europa.png',
        ]);
      
      self::setMeta('article', ['author' => $items[0]->user->canoniseName(),]);
      
      return \View::make('term',
        [
          'alphabet' => $alphabet,
          'term' => $items[0],
          'q' => $q,
          'letter' => $letter,
          'srcMarkup' => $srcMarkup
        ]);
    }
  
    /**
     * Do some text transformations
     * For instance, apply dropcaps
     *
     * @param string $src
     * @return string
     */
    public static function applyTextTransformations($src = '') {
      $ret = $src;
      $ret = DefinitioPublishingController::makeDropCaps($ret);
      
      return $ret;
    }
    
    /**
     * @param string|null $input
     * @return string
     * @throws \Exception
     */
    public static function parseDom($input = ''): string
    {
      $Parsedown = new ParsedownExtra();
      return $Parsedown->text($input);
    }
    
    /**
     * Add the Expand/collapse toggler
     *
     * @param string $s
     * @return string
     */
    public static function addExpands(string $s)
    {
      
      $ret = str_replace(
        '@expand',
        '<a class="toggler">Read more +</a><div class="expander is-hidden"',
        $s
      );
      
      return $ret;
    }
    
    /**
     * "Static" content such as Privacy, About Us, T&Cs
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function getShowContentStatic()
    {
      $slug = \Request::route()->uri;
      // get rid of - in about-us
      if (in_array($slug, self::$staticContent)) {
        $slug = str_replace('-', '', $slug);
      } else {
        abort(404);
      }
      
      $q = '';
      $letter = $q;
      $alphabet = \App\Alphabet::where('initial', '>=', '')->get();
      
      return \View::make('static.'.$slug, ['alphabet' => $alphabet, 'letter' => $letter]);
    }
    
    
    /**
     * Contact form in various modes
     * content/forms
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function contact()
    {
      return \View::make('contact',
        [
          'request' => \Request::route()->uri
        ]);
    }
  
    /**
     * Proof of concept - DAV filesystem access
     * Add the username at the beginning or specify in the prefux part
     * of the WebDAV config stanza
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getDAVFile() {
      // WebDAv
      $x = \Storage::disk('webdav')->get('pete/Betty.txt');
      var_dump($x);
      return;
    }
    
    /**
     * @param string $code
     */
    public function error($code = '500')
    {
      abort(intVal($code));
    }
  
    /**
     * @param string $path
     * @return \Illuminate\Contracts\View\View
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function test($path = '500')
    {
    
/*
      // https://definitio.div/storage/uploads/5379542306.jpg
      $fileURL = 'https://definitio.div/storage/uploads/5379542306.jpg';

      $client = new \GuzzleHttp\Client();

      $request = $client->request('GET', $fileURL, [
        'curl' => [
          CURLOPT_SSL_VERIFYPEER => false
        ]
      ]);
  
      $stream = $request->getBody()->getContents();
      var_dump($stream);
      die;*/
      
/*      $fileFromId = 'editor-at-definitio.org.jpg';
      $x = Storage::disk('sftp')->get($fileFromId);
      return response($x)->withHeaders(['Content-Type' => 'image/jpeg'] );*/
      
      return \View::make('test',
        [
          'uploader' => 'uppy',
          'host' => \Request::getHost()
        ]);
    }

  }
