<?php
  
  namespace App\Http\Controllers;
  
  use App\Definition;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Input;
  use ParsedownExtra;
  use League\HTMLToMarkdown\HtmlConverter;
  
  class DefinitioContentController extends Controller
  {
    /**
     * Get a list of terms to manage
     * With pagination and filtering
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function terms(Request $request)
    {
      $q = $request->get('q', '');
      
      if (null == $q)
        $q = '';
      
      $letter = $q;
      
      $terms = \App\Item::where('expression', 'like', "$q%")
        ->orderBy('expression')
        ->forPage(1, 12)
        ->get()
      ;
      
      $alphabet = \App\Alphabet::where('initial', '>=', '')->get();
      
      return \View::make('admin.terms',
        [
          'alphabet' => $alphabet,
          'letter' => $letter,
          'terms' => $terms,
          'total' => count($terms)
        ]);
    }
    
    /**
     * Get and display edit form for a single term
     *
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function term(Request $request, $id = '')
    {
      $q = Input::get('q', '');
      $method = 'PUT';
      
      if (null == $id || $id == '') {
        $term = new \App\Item();
        $method = 'POST';
      }
      else {
        $term = \App\Item::find($id);
        // throw exception if not
      }
      
      $letter = $q;
      $alphabet = \App\Alphabet::where('initial', '>=', '')->get();
      
      $labels = [];
      preg_match('/(?<=\[).+?(?=\])/', $term->src, $labels, PREG_OFFSET_CAPTURE);
      
      $term->content_html = self::prepareText($term->content_html);
      
      $src = [];
      preg_match('/(?<=\().+?(?=\))/', $term->src, $src, PREG_OFFSET_CAPTURE);
      
      $source = ['label' => [''], 'href' => ['']];
      if (count($labels) > 0) {
        $source['label'] = $labels[0];
      }
      
      if (count($src) > 0) {
        $source['href'] = $src[0];
      }
      
      //$term->published_at = '1973-11-17';
      if (!null == $term->published_at) {
        $term->published_at = Date('Y-m-d H:i:s', strtotime($term->published_at));
      }
      
      $related = [];
      foreach ($term->related as $rel) {
        $related[] = $rel->expression;
      }
      if ($request->get('v','')=='2') {
        return \View::make('admin.term_edit_2',
          [
            'term' => $term,
            'alphabet' => $alphabet,
            'letter' => $letter,
            'sourceLabel' => $source['label'][0],
            'sourceLink' => $source['href'][0],
            'related' => implode(',', $related),
            'method' => $method,
          ]);
        
      };
      return \View::make('admin.term_edit',
        [
          'term' => $term,
          'alphabet' => $alphabet,
          'letter' => $letter,
          'sourceLabel' => $source['label'][0],
          'sourceLink' => $source['href'][0],
          'related' => implode(',', $related),
          'method' => $method,
        ]);
      
    }
  
    /**
     * @param string $src
     * @return mixed|string
     * @throws \Exception
     */
    public static function prepareText($src = '') {
      $ret = $src;

      // convert <label class="label-red">Expand...</label>
      $ret = str_replace(
        "@expand",
        '<label class="label-red">Expand...</label>',
        $ret);
      
      return $ret;
    }
    
    /**
     * Handle update (and possibly insert) requests for single term,
     * usually invoked from a single term edit form requested with a GET /engine/term/ID
     *
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function putTerm(Request $request, $id = '')
    {
      $user = \Auth::User();
      if (!intval($user->id) > 0) {
        abort(403);
      }
      
      $validator = \Validator::make($request->all(), [
        'expression' => 'required|max:2550',
      ]);
      
      if ($validator->fails()) {
        dump($validator->errors());
        return redirect('/engine/term/' . $id)
          ->with('message', 'Well done!')
          ->withErrors($validator->errors())
          ;
      } else {
        $term = \App\Item::find($id);
        if (!$term) {
          abort(404);
        }
        
        $term->expression = $request->input('expression');
        
/*        // $converter = new HtmlConverter();
        $term->content_html = $request->input('content_html');
        
        // convert <label class="label-red">Expand...</label>
        $term->content_html = str_replace(
          '<label class="label-red">Expand...</label>',
          "@expand",
          $term->content_html);*/
        
        $term->published_at = $request->input('published_at');
        
        if ($request->input('link_href') !== '' && $request->input('link_label') !== '')
          $term->src = '[' . $request->input('link_label') . '](' . $request->input('link_href') . ')';
        
        try { //verticalismus. shift defs out in separate function, perhaps in the model (item) itself
          $term->save();
          
          // save definitions
          $keys = $request->keys();
          foreach ($keys as $key) {
            if (strpos($key, 'definition_heading') === 0) {
              $id = substr($key, strrpos($key, '_') + 1);
              $bodyField = "definition_text_$id";
              if (intval($id) > 0 ) {
                if (! in_array($bodyField, $keys)) {
                  abort('500', "The corresponding body isn't there for this definition");
                }
                
                $definition = Definition::find($id);
                $definition->heading = $request->$key;
                $definition->content_html = $request->$bodyField;
                try {
                  $definition->save();
                } catch (\Exception $e) {
                  abort('500', "Problems encountered trying to save definition #$id");
                }
              }
            }
          }
          
        } catch (\Exception $exception) {
          abort(500, $exception->getMessage());
        }
        
        if ($request->input('goPreview') == 'preview') {
          return redirect('/term/' . $term->slug)
            ->with('message', 'Term updated');
        }
        
        return redirect($request->getUri())
          ->with('message', 'Term updated');
      }
    }
    
    /**
     * Handle update (and possibly insert) requests for single term,
     * usually invoked from a single term edit form requested with a GET /engine/term/ID
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postTerm(Request $request)
    {
      $user = \Auth::User();
      if (!intval($user->id) > 0) {
        abort(403);
      }
      
      $validator = \Validator::make($request->all(), [
        'expression' => 'required|max:2550',
      ]);
      
      if ($validator->fails()) {
        return redirect()
          ->back()
          ->withErrors($validator->errors())
          ;
      } else {
        $term = new \App\Term();
        $term->expression = $request->input('expression');
        
        $term->content_html = $request->input('content_html');
        
        $term->published_at = $request->input('published_at');
  
        if ($request->input('link_href') !== '')
          $term->src = '[' . $request->input('link_label') . '](' . $request->input('link_href') . ')';
        
        // handle versioning!
        $term->user_id = $user->id;
        
        $term->save();
        
        if ($request->input('goPreview')) {
          $item = \App\Item::find($term->id);
          
          return redirect('/term/' . $item->slug)
            ->with('message', 'Well done!');
        }
        
        return redirect('/engine/term/' . $term->id)
          ->with('message', 'Well done!');
        
      }
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyTerm(Request $request, $id = '')
    {
      $term = \App\Item::find($id);
      if (!$term || $id == '') {
        abort(404);
      }
      
      $term->deleted_at = date("Y-m-d H:i:s");
      $term->save();
      
      return redirect()
        ->back()
        ->with('message', 'Term deleted')
        ;
    }
    
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reviveTerm(Request $request, $id = '')
    {
      $term = \App\Item::find($id);
      if (!$term || $id == '') {
        abort(404);
      }
      
      $term->deleted_at = null;
      $term->save();
      
      return redirect()
        ->back()
        ->with('message', 'Term brought back from the dead')
        ;
    }
    
    /**
     * Dispatch request accordingly
     * Dispatcher needed in order to work around the fact that
     * PUT method with Laravel really is a GET request with all the query string consequences..
     *
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function methodDispatcher(Request $request, $id = '')
    {
      if ($id == '') {
        return $this->postTerm($request);
      }
      else {
        return $this->putTerm($request, $id);
      }
    }
  
    /**
     * @param $d
     */
    public static function publishedAsString($d)
    {
    
    }
    
    public function tag(Request $request)
    {
      dd('tag');
    }
  
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function tags(Request $request)
    {
      $q = Input::get('q', '');
      
      if (null == $q)
        $q = '';
      
      $letter = $q;
      
      $terms = \App\Item::where('expression', 'like', "$q%")
        ->orderBy('expression')
        ->get()
      ;
      
      $alphabet = \App\Alphabet::where('initial', '>=', '')->get();
      
      return \View::make('admin.tags',
        [
          'alphabet' => $alphabet,
          'letter' => $letter,
          'items' => $terms,
          'total' => count($terms)
        ]);
    }
    
    /**
     * @param string $input
     * @return string
     * @throws \Exception
     */
    public static function parseDom(string $input): string
    {
      $Parsedown = new ParsedownExtra();
      return $Parsedown->text($input);
    }
  }
