<?php

namespace App\Http\Controllers;

use App\Helpers\TuttiFrutti;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    private $request;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Get photos
     * Plug a repo eventually (s3, flickr, google, etc..)
     *
     * @param string $minus
     * @return array
     */
    public static function getPhotos($minus = '')
    {
        $root = base_path('/public/images/photos');
        $scanned_directory = array_diff(scandir($root), ['..', '.', '.DS_Store', $minus]);
        return $scanned_directory;
    }
    
    /**
     * Generate an image of specific dimensions, dpi etc from a URI/file
     * Uses the proxied imaginary "microservice" which in turn uses libvips.
     * In addition, the streams get stored locally to serve future requests.
     *
     * @param $width
     * @param $height
     * @param $dpi
     * @param $clrspace
     * @param $source
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function makeImage($width, $height, $dpi, $clrspace, $path)
    {
        // "preview/s3/assets/photos/_DSF4922.jpg"
        $source = substr($path, strrpos($path, '/')+1);
        $fname = substr($source, 0, strpos($source, '.'));
        $ext = substr($path, strpos($path, '.')+1);

/*        $urlPrefix = $this->request->getScheme().
            '://'.
            $this->request->getHost();
*/
        $urlPrefix = 'https://www.definitio.org';
        
        if (in_array(strtolower($ext), ['jpg', 'jpeg'])) {
            $type = 'jpeg';
        } elseif (strtolower($ext) === 'png') {
            $type = 'png';
        } else {
            abort(566);
        }
        
        $format = '%s-w%d-h%d-d%d-c%s.%s';
        $target = sprintf($format, $fname, $width, $height, $dpi, $clrspace, $ext);

        if (file_exists(base_path("public/images/scaled/$target"))) {
            return response()->file(base_path("public/images/scaled/$target"));
        }
        
        // construct an imaginary url
        // factory/smartcrop?colorpsace=bw&dpi=150&width=320&height=220&type=jpeg&file=photos/{{$img}}
        $url = '/factory/resize'
            .'?width='.$width
            .'&height='.$height
            .'&dpi='.$dpi
            .'&colorspace='.$clrspace
            .'&type='.$type
            .'&url='.$urlPrefix.'/'.$path;

        
        $stream = TuttiFrutti::httpRequest($url)
            ->getBody()
            ->getContents();
        
        file_put_contents(base_path("public/images/scaled/$target"), $stream);
        return response()->file(base_path("public/images/scaled/$target"));
    }
    
    /**
     * Generate an image of specific dimensions, dpi etc from a URI/file
     * Uses the free service provided by weserv.nl
     * The service itself takes care of proxying
     *
     * @todo add filters (other than bw, e.g. sepia, duotone etc.)
     *
     * @param $width
     * @param $height
     * @param $dpi
     * @param $clrspace
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function proxyImage($width, $height, $dpi, $clrspace, $path)
    {
        $source = substr($path, strrpos($path, '/')+1);
        $fname = substr($source, 0, strpos($source, '.'));
        $ext = substr($source, strpos($source, '.') + 1);
    
        if (in_array(strtolower($ext), ['jpg', 'jpeg'])) {
            $type = 'jpeg';
        } elseif (strtolower($ext) === 'png') {
            $type = 'png';
        } else {
            abort(566);
        }
    
        $format = '%s-w%d-h%d-d%d-c%s.%s';
        $target = sprintf($format, $fname, $width, $height, $dpi, $clrspace, $ext);
    
        if (file_exists(base_path("public/images/scaled/$target"))) {
            return response()->file(base_path("public/images/scaled/$target"));
        }
        
        $url = 'https://images.weserv.nl/?url=www.diveris.org/images'
            . '/' . $path . '/' . $source
            . '&w=' . $width
            . '&h=' . $height
            . '&dpr=' . $dpi
            . '&fit=inside'
            . '&a=focal';
    
        if ($clrspace === 'bw') {
            $url .= '&filt=greyscale';
        } elseif ($clrspace !== '') {
            $url .= '&filt='.$clrspace;
        }
        
        $stream = TuttiFrutti::httpRequest($url)
            ->getBody()
            ->getContents();
    
        file_put_contents(base_path("public/images/scaled/$target"), $stream);
        
        return response($stream)
            ->header('Content-Type', "image/$type");
    }
    
    /**
     * Check if image is landscape
     *
     * @param string $image
     * @return bool
     */
    public static function isLandscape(string $image)
    {
        $filename = base_path('public/'.$image);
        $size = getimagesize($filename);
        if (!is_array($size) || count($size) < 1) {
            abort(554);
        }
        return $size[0] > $size[1];
    }
    
    /**
     * Check if image is portrait
     *
     * @param string $image
     * @return bool
     */
    public static function isPortrait(string $image)
    {
        return !self::isLandscape($image);
    }
}
