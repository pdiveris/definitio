<?php
  
  namespace App\Http\Middleware;
  
  use Closure;
  
  class CaptureReferrals
  {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if('' !== $request->get('fbclid', '')) {
        $interaction = new \App\Interaction();
        
        $interaction->ip_address = $request->ip();
        $interaction->request_type = 'view';
        $interaction->referrer = 'facebook';
        $interaction->ref_key = $request->get('fbclid', 'horror');
        $interaction->header = $request->headers;
        
        $interaction->save();
      }
      return $next($request);
    }
  }
