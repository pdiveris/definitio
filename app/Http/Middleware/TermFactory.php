<?php
  
  namespace App\Http\Middleware;
  
  use App\Item;
  use Closure;
  
  class TermFactory
  {
    /**
     * Handle an incoming request.
     *
     * This handler is specific to items.
     * It will generate JSON, PNG, PDF or XML response depending on
     * the "extension" specified as a suffix in the item's slug.
     * So a request for /term/john-a-study-in-late-psychopathy.png will
     * return an image of the item's print view
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // https://definitio.div/term/john-a-study-in-late-psychopathy.png
      
      $workingUri = str_replace($request->getHost(), '', $request->getUri());
      $workingUri = str_replace($request->getScheme(), '', $workingUri);
      $workingUri = str_replace('://', '', $workingUri);
      
      $bits = explode('/', substr($workingUri,1));
      
      if (count($bits)==2 && $bits[0]=='term') {
        $resource = $bits[1];
        if (strpos($resource, '.')==true) {
          $extension = substr($resource, strpos($resource, '.') + 1);
          $red = $request->getScheme();
          $red .= '://';
          $red .= $request->getHost();
          $red .= '/';
          
          switch ($extension) {
            case 'png':
              $red .= 'term/';
              $red .= substr($bits[1], 0, strpos($bits[1], '.') );
              
              return response()->file(\App\Http\Controllers\MediaController::takeSaveAndReturnSnapFromUrl($red));
            case 'pdf':
              $red .= 'pdf/term/';
              $red .= substr($bits[1], 0, strpos($bits[1], '.') );

              $ctrl =  \App::make('\App\Http\Controllers\DefinitioPublishingController');
             
              return $ctrl->pdfWebkit($red, substr($bits[1], 0, strpos($bits[1], '.') ));
            case 'json':
              // $x = Item::findOneBySlug(str_replace('.json', '', $bits[1]) );
              return response()
                ->json(Item::findOneBySlug(str_replace('.json', '', $bits[1]) )->getAttributes())
                ->header('Content-Type', 'application/json');
            case 'xml':
              $x = Item::findOneBySlug(str_replace('.xml', '', $bits[1]) );
              return response()->xml([
                'status' => 'success',
                'data' => [
                    $x->getAttributes()
                  ]
                ]
              );
            case 'rss':
              // todo
              // implement RSS
              
            }
        } else {
          //shouldn't get in here
        }
      }
      return $next($request);
    }
  }
