<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 2019-02-17
   * Time: 19:27
   */
  
  namespace App;
  
  use Illuminate\Database\Eloquent\Model;

  /**
   * Class UserModel
   * @package App
   *
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor newModelQuery()
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor newQuery()
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor query()
   * @mixin \Eloquent
   *
   * @property int $id
   * @property string $preferences
   *
   * @method static \Illuminate\Database\Eloquent\Builder|\App\Editor whereId($value)
   *
   */
  class UserModel extends Model
  {
    // Cast attributes JSON to array
    // See https://scotch.io/tutorials/working-with-json-in-mysql
    protected $casts = [
      'preferences' => 'array'
    ];

    public function __construct(array $attributes = [])
    {
      parent::__construct($attributes);
    }
  
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
      return $this->belongsToMany(Role::class, 'role_user', 'user_id');
    }
  
    /**
     * Check one role
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
      foreach ( $this->roles as $check) {
        if ($check->name == $role)
          return true;
      }
      return false;
    }
    
    /**
     * @param $key
     * @return mixed|null
     */
    public function getPreference($key)
    {
      if (is_array($this->preferences) && array_key_exists($key, $this->preferences) ) {
        if (is_array($this->preferences[$key]))
          return $this->preferences[$key][0];
        
        return $this->preferences[$key];
      }
      return null;
    }
  
    public function A__call($name, $arguments)
    {
      // Note: value of $name is case sensitive.
      echo "Calling object method '$name' "
        . implode(', ', $arguments). "\n";
    }
  
    /**  As of PHP 5.3.0  */
    public static function A__callStatic($name, $arguments)
    {
      // Note: value of $name is case sensitive.
      echo "Calling static method '$name' "
        . implode(', ', $arguments). "\n";
    }
    
    public function getPreferredEditor() {
      $obj = json_decode($this->preferences);
      if (isset($obj->editor) && is_array($obj->editor) && count($obj->editor) > 0) {
        return $obj->editor[0];
      }
      return 'standard';
    }

    
  }
