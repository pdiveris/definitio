<?php
  
  namespace App\Providers;
  
  use Illuminate\Support\ServiceProvider;
  use League\Flysystem\Cached\Storage\Memory;
  use Storage;
  use League\Flysystem\Filesystem;
  use League\Flysystem\Cached\CachedAdapter;
  
  
  class CachedFileSystemProvider extends ServiceProvider
  {
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      //
    }
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      Storage::extend('s3-cached', function ($app, $config) {
        $adapter = $app['filesystem']->createS3Driver($config);
        // $store = new Memory();
        // $store = new
        
        return new Filesystem(new CachedAdapter($adapter->getDriver()->getAdapter(), $store));
      });
    }
  }
