<?php

namespace App\InertiaPlus;

use Illuminate\Support\Facades\Facade;

/**
 * @method static void setRootView($name)
 * @method static void share($key, $value = null)
 * @method static array getShared($key = null)
 * @method static void version($version)
 * @method static int|string getVersion()
 * @method static \App\InertiaPlus\ResponsePlus render($component, $props = [], $modal = false)
 *
 * @see \Inertia\ResponsePlusFactory
 */
class InertiaPlus extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ResponsePlusFactory::class;
    }
}
