<?php
  
  namespace App;
 
  /**
   * App\SiteUser
   *
   * @method static \Illuminate\Database\Eloquent\Builder|\App\SiteUser newModelQuery()
   * @method static \Illuminate\Database\Eloquent\Builder|\App\SiteUser newQuery()
   * @method static \Illuminate\Database\Eloquent\Builder|\App\SiteUser query()
   * @mixin \Eloquent
   *
   * @property int $id
   * @property string $name
   * @property string $email
   * @property string $password
   * @property string $handle
   * @property string $preferences
   * @property int $enabled
   * @property string $email_verified_at
   * @property string $old_email
   *
   * @method static \Illuminate\Database\Eloquent\Builder|\App\SiteUser whereEmail($value)
   * @method static \Illuminate\Database\Eloquent\Builder|\App\SiteUser whereId($value)
   * @method static \Illuminate\Database\Eloquent\Builder|\App\SiteUser whereName($value)
   * @method static \Illuminate\Database\Eloquent\Builder|\App\SiteUser whereHandle($value)
   */
  class SiteUser extends UserModel
  {
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    
  }
