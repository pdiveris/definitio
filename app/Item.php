<?php
  
  namespace App;
  
  use Illuminate\Database\Eloquent\Model;

  /**
 * Class Item
 *
 * @package App
 * @property int $id
 * @property string|null $expression
 * @property string|null $slug
 * @property string|null $classic
 * @property string $content_html
 * @property string $content_markdown
 * @property string|null $definitio
 * @property string|null $src
 * @property int|null $user_id
 * @property string|null $time_point
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $published_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Definition[] $definitions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Related[] $related
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereClassic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereContentHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereContentMarkdown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereDefinitio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereExpression($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereTimePoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUserId($value)
 * @mixin \Eloquent
 */
  class Item extends Model
  {
    //
    /**
     * Get the comments for the blog post.
     */
    public function definitions()
    {
      return $this->hasMany('App\Definition');
    }
  
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
      return $this->belongsToMany(Tag::class);
    }
  
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function related() {
      return $this->belongsToMany(Related::class);
    }
  
    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
      return $this->belongsTo('App\User');
    }
  
    /**
     * @param string $slug
     * @return \App\Item|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public static function findOneBySlug(string $slug) {
      $ret = self::where('slug', '=', $slug)->first();
      if (null == $ret) {
        $ret = new static();
      }
      return $ret;
    }
    
  }
