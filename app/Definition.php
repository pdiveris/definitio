<?php
  
  namespace App;
  
  use Illuminate\Database\Eloquent\Model;
  
  /**
 * App\Definition
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $heading
 * @property int|null $item_id
 * @property string|null $author
 * @property string|null $body
 * @property string|null $content_html
 * @property string|null $content_markdown
 * @property string|null $time_point
 * @property string|null $publication
 * @property string|null $src
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereContentHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereContentMarkdown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereHeading($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition wherePublication($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereTimePoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Definition whereUserId($value)
 */
class Definition extends Model
  {
    //
    
  }
