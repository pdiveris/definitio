<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Related
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Item[] $items
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $expression
 * @property string|null $classic
 * @property string|null $definitio
 * @property string|null $src
 * @property int|null $user_id
 * @property string|null $time_point
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereClassic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereDefinitio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereExpression($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereTimePoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Related whereUserId($value)
 */
class Related extends Model
{
    //
  protected $table = 'related';
  
  public function items() {
    return $this->belongsToMany('App\Item');
  }
  
}
