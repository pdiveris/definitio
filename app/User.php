<?php
  
  namespace App;
  
  use Illuminate\Notifications\Notifiable;
  use Illuminate\Contracts\Auth\MustVerifyEmail;
  use Illuminate\Foundation\Auth\User as Authenticatable;
  
 /**
 * App\User
 *
 */
class User extends Authenticatable
  {
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'email', 'password',
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password', 'remember_token',
    ];
  
    public function roles()
    {
      return $this->belongsToMany(Role::class);
    }

    /**
     * @param string|array $roles
     * @return bool
     */
    public function authorizeRoles($roles)
    {
      if (is_array($roles)) {
        return $this->hasAnyRole($roles) ||
          abort(401, 'This action is unauthorized.');
      }
      return $this->hasRole($roles) ||
        abort(401, 'This action is unauthorized.');
    }
  
    /**
     * Check multiple roles
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole($roles)
    {
      return null !== $this->roles()->whereIn(‘name’, $roles)->first();
    }
  
    /**
     * Check one role
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
      foreach ( $this->roles as $check) {
        if ($check->name == $role)
          return true;
      }
      return false;
    }
  
    /**
     * @param $term
     * @return bool
     */
    public function userHasAdminAccessToTerm($term) {
      return !null == $this && $this->hasRole('administrator') || $this->id == $term->user_id;
    }
    
    public function canoniseName() {
      return strtolower(str_replace(' ', '', $this->name));
    }
    
    public static function listEditors() {
    
    }
  }
