<?php
  
  namespace App;
  
  use Illuminate\Database\Eloquent\Model;
  
  /**
 * App\Alphabet
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Alphabet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Alphabet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Alphabet query()
 * @mixin \Eloquent
 */
class Alphabet extends Model
  {
    //
    protected $table = 'alphabet';
  }
