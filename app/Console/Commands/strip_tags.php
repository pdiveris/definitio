<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class strip_tags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'definitio:strip-tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the terms\' body with raw text (strip tags)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $terms = \App\Term::all();
        $i = 0;
        foreach ($terms as $term) {
            $i++;
            echo 'Updated '.$term->expression. PHP_EOL;
            $term->body = strip_tags($term->content_html);
            $term->save();
        }
        echo sprintf('Updated %d terms', $i);
    }
}
