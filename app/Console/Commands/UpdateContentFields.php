<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Markdownify\ConverterExtra;

class UpdateContentFields extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'definitio:contentsync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the markdown and content fields from classic';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
      echo "Updating terms: setting content_html to classic and content_markdown to parse(classic)\n";
      $terms = \App\Term::all();
      $converter = new ConverterExtra;
      
      foreach ($terms as $i => $term) {
        echo 'Doing ';
        echo $term->id;
        echo ' ';
        echo $term->expression;
        echo "...\n";
        
        $term->content_html = $term->classic;
        $term->content_markdown = $converter->parseString($term->classic);
        $term->save();
      }
      echo "Done\n";
    }
}
