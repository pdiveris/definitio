<?php
  
  namespace App\Console\Commands;
  
  use Illuminate\Console\Command;
  
  class UpdateItemsCache extends Command
  {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'definitio:itemscache
      {baseUrl?}
    ';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the cached PMG and PDF snaps of the items';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
      parent::__construct();
    }
  
    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
      
      
      if (null == $this->argument('baseUrl')) {
        $baseUrl = $this->ask('Base url (e.g. http://definitio.div ?');
      } else {
        $baseUrl = $this->argument('baseUrl');
      }
      
      $this->info("OK, working on $baseUrl");

      
      //
      $terms = \App\Item::all();
  
      $client = new \GuzzleHttp\Client();
  
      foreach ($terms as $i => $term) {
        echo 'Doing ';
        echo $term->id;
        echo ' ';
        echo $term->expression;
        echo "...\n";
        
        if (file_exists(storage_path('/artifacts/dompdf/'.$term->slug.'.pdf'))) {
          echo "\n>>>>>>>>>> deleting ".$term->slug.'.pdf';
          unlink(storage_path('/artifacts/dompdf/'.$term->slug.'.pdf'));
        }
  
        if (file_exists(public_path('/assets/snaps/'.$term->slug.'.png'))) {
          echo "\n>>>>>>>>>> deleting ".$term->slug.'.png';
          unlink(public_path('/assets/snaps/'.$term->slug.'.png'));
        }
  
        $url = $baseUrl;
        $url .= '/';
        $url .= 'term/'.$term->slug.'.pdf';
  
        echo "\n$url\n";
  
        $response = $client->request('GET', $url, [
          'curl' => [
            CURLOPT_SSL_VERIFYPEER => false
          ]
        ]);
  
        echo "\nCode: ".$response->getStatusCode()."\n";
  
        $url = $baseUrl;
        $url .= '/';
        $url .= 'term/'.$term->slug.'.png';
  
        echo "\n$url\n";
  
        $response = $client->request('GET', $url, [
          'curl' => [
            CURLOPT_SSL_VERIFYPEER => false
          ]
        ]);
        
        echo "\nCode: ".$response->getStatusCode()."\n";
      }
    }
  }
