<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\link
 *
 * @property int $id
 * @property string $label
 * @property string|null $type
 * @property string|null $publisher
 * @property string|null $isbn
 * @property string|null $comments
 * @property string $uri
 * @property int $user_id
 * @property int $term_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereComments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Link wherePublisher($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereIsbn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereTermId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\link whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Link extends Model
{
    //
}
