<?php
  
  namespace App;
  
  use Illuminate\Database\Eloquent\Model;
  //  use App\ModelWithHints;

  /**
 * Class Term
 *
 * @package App
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $expression
 * @property string|null $slug
 * @property string|null $content_html
 * @property string|null $content_markdown
 * @property string|null $definitio
 * @property string|null $src
 * @property int|null $user_id
 * @property string|null $time_point
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $published_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Definition[] $definitions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Related[] $related
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereDefinitio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereExpression($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereTimePoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereContentHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Term whereContentMarkdown($value)
 */
  class Term extends Model
  {
    //
    /**
     * Get the comments for the blog post.
     */
    public function definitions()
    {
      return $this->hasMany('App\Definition');
    }
  
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
      return $this->belongsToMany(Tag::class);
    }
  
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function related() {
      return $this->belongsToMany(Related::class);
    }
  
    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
      return $this->belongsTo('App\User');
    }
  }
