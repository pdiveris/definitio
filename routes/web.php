<?php
  
  /*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
  */
  
  $this->get('logout', 'Auth\LoginController@logout')->name('logout');
  $this->get('login', 'Auth\LoginController@login')->name('login');
  
  // new, PHP Tus Server by ankitopokhel!
  // routes/web.php
  Route::any('/tus/{any?}', function () {
    $response = app('tus-server')->serve();
    
    return $response->send();
  })->where('any', '.*');
  
  Route::any('/i', function () {
    phpinfo();
    return '';
  });
  
  Route::get('/', 'DefinitioSiteController@alphabet')->name('entries');
  Route::get('/alphabet', 'DefinitioSiteController@alphabet')->name('alphabet');
  Route::get('/term', 'DefinitioSiteController@term')->name('term')->middleware(['capture.referrals']);
  Route::get('/term/{slug?}', 'DefinitioSiteController@term')->name('term')->middleware(['capture.referrals']);
  Route::get('/become', 'DefinitioSiteController@contact')->name('become');
  Route::get('/suggest', 'DefinitioSiteController@contact')->name('suggest');
  Route::get('/privacy', 'DefinitioSiteController@getShowContentStatic')->name('privacy');
  Route::get('/about-us', 'DefinitioSiteController@getShowContentStatic')->name('about-us');
  Route::get('/aboutus', 'DefinitioSiteController@getShowContentStatic')->name('aboutus');
  Route::get('/contact-us', 'DefinitioSiteController@getShowContentStatic')->name('contact-us');
  Route::get('/contactus', 'DefinitioSiteController@getShowContentStatic')->name('contactus');
  Route::get('/home', 'DefinitioSiteController@alphabet')->name('home');
  Route::get('/error/{code?}', 'DefinitioSiteController@error')->name('error');
  Route::get('/test/{whatever?}', 'DefinitioSiteController@test')->name('test');
  
  Route::get('/avatars/{id?}', 'StorageController@getAvatar')->name('avatars/get');
  
  Route::get('/fascia', 'DefinitioSiteController@fascia')->name('fascia');
  Route::get('/dropcaps', 'DefinitioSiteController@dropCap')->name('drop');
  
  Route::get('/pdf', 'DefinitioPublishingController@pdf')->name('pdf');
  // Route::get('/pdftoo/term/{id?}', 'DefinitioPublishingController@pdfToo')->name('pdfToo');
  
  Route::get('/mpdf/term/{id?}', 'DefinitioPublishingController@pdfMpdf')->name('publish/pdfMpdf');
  
  Route::get('/pdf/term/{id?}', 'DefinitioPublishingController@pdfWebkit')->name('publish/pdf');
  
  Route::get('/dompdf/term/{id?}', 'DefinitioPublishingController@pdfDompdf')->name('publish/pdfDompdf');
  Route::get('/publish/term/{id?}', 'DefinitioPublishingController@publish')->name('publish/html');
  
  Route::get('/send/{title?}/{content?}', 'EmailController@getSend')->name('send');
  Route::post('/send', 'EmailController@send')->name('postSend');
  Route::get('/snap', 'MediaController@getUrlSnapshot')->name('snap');
  
  Route::get('/terms', 'DefinitioContentController@terms')->name('terms');
  Route::get('/tags', 'DefinitioContentController@tags')->name('tags');
  
  // engine
  Route::group(['prefix' => 'engine', 'middleware' => 'auth'], function () {
    Route::get('term/{id?}', 'DefinitioContentController@term')->name('define')->middleware('auth');
    Route::post('term/{id?}', 'DefinitioContentController@methodDispatcher')->name('define');
    Route::post('term/destroy/{id?}', 'DefinitioContentController@destroyTerm')->name('term/destroy');
    Route::post('term/revive/{id?}', 'DefinitioContentController@reviveTerm')->name('term/revive');
    Route::get('tag', 'DefinitioContentController@tag')->name('tag')->middleware('auth');
  });
  
  //backstage (aka admin)
  Route::group(['prefix' => 'backstage', 'middleware' => 'auth'], function () {
    Route::get('/', 'DefinitioApplicationManager@home')->name('backstage/home');
    Route::get('user/{id?}', 'DefinitioApplicationManager@user')->name('backstage/user');
    Route::post('user/{id?}', 'DefinitioApplicationManager@methodDispatcher')->name('backstage/user');
    Route::post('user/destroy/{id?}', 'DefinitioApplicationManager@destroyUser')->name('user/destroy');
    Route::post('user/revive/{id?}', 'DefinitioApplicationManager@reviveUser')->name('user/revive');
    Route::get('users/{params?}', 'DefinitioApplicationManager@users')->name('backstage/users');
    
    Route::get('redis/{store?}/{key?}/', 'StorageController@redis')->name('backstage/redis');
    Route::post('redis/{store?}/{key?}/', 'StorageController@postRedis')->name('backstage/postRedis');
  });
  
  //backstage (aka admin)
  Route::group(['prefix' => 'stream', 'middleware' => 'auth'], function () {
    Route::get('/', 'StreamController@index@home')->name('stream');
//    Route::get('user/{id?}', 'DefinitioApplicationManager@user')->name('backstage/user');
  });
  
  
  Route::group(['prefix' => 'ops'], function () {
    Route::post('filepersist/{id?}', 'DefinitioApplicationManager@filePersist')->name('ops/filepersist');
  });
  
  Route::any('stream', 'StreamController@index');
  Route::any('stream/client', 'StreamController@client');
  Route::any('tristram/{suffix?}', 'StreamController@tristram')->name('tristram');
  
  Auth::routes();

