@extends('layouts.app')
@section('beef')
  @include('partials.alphabet', array('alphabet' => $alphabet, 'letter'=>$letter, 'goto'=>'backstage/users', 'column'=>'first'))
  <section class="section">
    <div class="container">
      <div class="columns is-gapless">
        <div class="column">
          <a class="button is-medium is-pulled-right is-black is-inline-block-desktop" href="{{route('backstage/user')}}/">
            <i class="fas fa-plus"></i>
          </a>
        </div>
      </div>
      <div class="columns">
        <div class="column">
          @foreach($users as $user)
            <div class="box">
              <article class="media">
                <div class="media-left">
                  <figure class="image is-64x64">
                    <img src="{{ \App\Helpers\Utils::gravata($user->email, 64) }}">
                  </figure>
                </div>
                <div class="media-content">
                  <div class="content">
                    <p>
                      <a
                        class="button is-medium is-inline-block-desktop"
                        href="{{route('backstage/user')}}/{{$user->id}}"
                        style="margin-top: -10px;">
                        {{$user->name}}

                      </a>
                      <small>@ {{ $user->handle }}</small>

                    </p>
                  </div>
                </div>
              </article>
            </div>
          @endforeach
        </div>
      </div>

    </div>
  </section>

@stop
