@extends('layouts.app')
@section('beef')
  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <a class="button is-black is-medium is-rounded" href="{{route('backstage/users')}}">
            <i class="fas fa-chevron-left is-size-7"></i>
          </a>
        </div>
      </div>
    </div>
  </section>
  <div class="container " id="apostrophe">
    <div class="columns">
      <div class="column is-three-quarters">
        <h3 class="title is-3 avant " id="name_title" style="min-height: 1.2em;">{{$formUser->name}}</h3>
        <article class="media">
          <div class="media-content">
            <section class="section">
              <form method="POST" action="{{route('backstage/user')}}/{{$formUser->id}}">
                @csrf
                <div class="field ">
                  <label class="label is-size-5">Name</label>
                  @if($errors->has('name'))
                    <p class="help is-danger toclear" id="help_name">
                      {{$errors->first('name')}}
                    </p>
                  @endif
                  <div class="control is-expanded">
                    <input
                      class="input subtitle is-info didot"
                      type="text"
                      value="{{ old('name') != '' ? old('name') : $formUser->name }}"
                      name="name"
                      id="user_name"
                      onfocus="fixBlank()"
                      onkeydown="$('#help_name').text('')"
                      onkeyup="updateLabel()"
                      placeholder="{{$formUser->name}}" hidden
                    />
                  </div>
                </div>
                <div class="field ">
                  <label class="label is-size-5">Email</label>
                  @if($errors->has('email'))
                    <p class="help is-danger toclear" id="help_email" style="margin-top: -40px;">
                      {{$errors->first('email')}}
                    </p>
                  @endif
                  <div class="control has-icons-right" id="pass_panel_1">
                    <input
                      class="input subtitle is-info didot"
                      value="{{old('email') != '' ? old('email') : $formUser->email}}"
                      name="email"
                      id="user_email"
                      type="text"
                      onkeydown="$('#help_email').text('')"
                      placeholder="{{$formUser->email}}" hidden
                    />
                    <span class="icon is-small is-right">
                      <i class="fas fa-check"></i>
                    </span>
                  </div>
                </div>
                <div class="field ">
                  <label class="label is-size-5">Password</label>
                  @if($errors->has('password'))
                    <p class="help is-danger toclear" id="help_password">
                      {{$errors->first('password')}}
                    </p>
                  @endif
                  <div class="control has-icons-right">
                    <input
                      class="input subtitle is-info didot"
                      value=""
                      name="password"
                      id="user_password"
                      type="password"
                      onkeydown="$('#help_password').text('')"
                      placeholder="Password" hidden
                    />
                    <span class="icon is-small is-right">
                        <i class="fas fa-check"></i>
                    </span>
                  </div>
                </div>
                <div class="field ">
                  <label class="label is-size-5">Social media handle</label>
                  @if($errors->has('handle'))
                    <p class="help is-danger" id="help_handle">
                      {{$errors->first('handle')}}
                    </p>
                  @endif
                  <div class="control">
                    <input
                      class="input subtitle is-info didot"
                      value="{{old('handle') != '' ? old('handle') : $formUser->handle}}"
                      name="handle"
                      id="user_handle"
                      onkeydown="$('#help_handle').text('')"
                      type="text"
                      placeholder="{{$formUser->handle}}" hidden
                    />
                  </div>
                </div>

                <!--start panel --->
                <nav class="panel">
                  <p class="panel-heading">
                    <label class="label is-size-5">Preferences</label>
                  </p>
                  <label class="panel-block">
                    <div class="field">
                      <label class="label">Editor</label>
                      <div class="control ">
                        <div class="field">
                          <input
                            class="is-checkradio"
                            id="preferencesEditorHtml"
                            type="radio"
                            name="preferencesEditor"
                            value="html"
                            {{$formUser->getPreference('editor') == 'html' ? 'checked' : '' }}
                          >
                          <label for="preferencesEditorHtml">HTML</label>
                          <input
                            class="is-checkradio"
                            id="preferencesEditorMarkdown"
                            type="radio"
                            name="preferencesEditor"
                            value="markdown"
                            {{$formUser->getPreference('editor') == 'markdown' ? 'checked' : '' }}
                          >
                          <label for="preferencesEditorMarkdown">Markdown</label>
                        </div>
                      </div>
                    </div>
                  </label>
                  <label class="panel-block">
                    <div class="field">
                      <label class="label">Spelling</label>
                      <div class="control">
                        <div class="field">
                          <input
                            id="switchSpellingChecker"
                            type="checkbox"
                            name="preferencesSpellingChecker"
                            class="switch is-rtl is-black"
                            {{$formUser->getPreference('spelling_checker') ? 'checked' : '' }}
                          >
                          <label for="switchSpellingChecker">On/Off</label>
                        </div>
                      </div>
                    </div>

                  </label>
                </nav>
                <!-- end panel --->
                <!--start panel --->
                <nav class="panel">
                  <p class="panel-heading">
                    <label class="label is-size-5">Attributes</label>
                  </p>
                  <label class="panel-block">
                    <div class="field">
                      <label class="label">Is active</label>
                      <div class="control">
                        <div class="field">
                          <input
                            id="switchActive"
                            type="checkbox"
                            name="enabled"
                            class="switch is-rtl is-black"
                            {{$formUser->enabled || null === $formUser->id ? 'checked' : '' }}
                          >
                          <label for="switchActive">On/Off</label>
                        </div>
                      </div>
                    </div>
                  </label>
                  <label class="panel-block">
                    <div class="field">
                      <label class="label">User roles</label>
                      <div class="control">
                        <div class="field">
                          @foreach(\App\Role::all() as $i=>$role)
                            <input
                              class="is-checkradio is-block {{$role->id == 1 ? 'is-danger' : 'is-info'}}"
                              id="roleCheckbox_{{$role->name}}"
                              type="checkbox"
                              value="{{$role->id}}"
                              name="roleCheckbox[{{$role->name}}]"
                              {{$formUser->hasRole($role->name) ? 'checked' : '' }}
                            >
                            <label for="roleCheckbox_{{$role->name}}">{{ucfirst($role->name)}}</label>
                          @endforeach
                        </div>
                      </div>
                    </div>

                  </label>
                </nav>
                <!--start injection --->

                <div class="field" style="margin-top: 2.2em;">
                  <div class="control ">
                    <button type="submit" class="button is-success">
                      Update
                    </button>
                    <input
                      class="is-checkradio is-inline pull-left"
                      id="goPreview"
                      type="checkbox"
                      name="goPreview"
                      value="preview"
                    />
                  </div>
                </div>
              </form>
              @if($method=='PUT')
                <form method="POST"
                      action="{{ $formUser->deleted_at == null ? route('user/destroy') : route('user/revive')}}/{{$formUser->id}}">
                  @csrf
                  @if(null == $formUser->deleted_at)
                    <button type="submit" class="button is-danger is-pulled-right" style="margin-top: -2.4em;">
                      Delete
                    </button>
                  @else
                    <button type="submit" class="button is-warning is-pulled-right" style="margin-top: -2.4em;">
                      Undelete
                    </button>
                  @endif
                </form>
              @endif
            </section>
          </div>
        </article>
      </div>
      <div class="column">
        <figure class="media-left">
          <p class="image is-256x256">
            <img src="{{ \App\Helpers\Utils::gravata($formUser->email, 256) }}" id="user_image">
          </p>
        </figure>
        <div>
          <div id="drag-drop-area"></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function fixBlank() {
      $('#user_name').val($('#user_name').val().trim());
    }

    function updateLabel() {
      $('#name_title').html($('#user_name').val());
    }

    const Webcam = Uppy.Webcam;
    const Dashboard = Uppy.Dashboard;

    var uppy = Uppy.Core()
      .use(Uppy.Dashboard, {
        inline: true,
        target: '#drag-drop-area',
        note: 'Images and video only, 2–3 files, up to 1 MB',
        width: 260,
        height: 470,
        metaFields: [
          {id: 'name', name: 'Name', placeholder: 'file name'},
          {id: 'caption', name: 'Caption', placeholder: 'describe what the image is about'}
        ],

      })
      .use(Webcam, {target: Dashboard})
      .use(Uppy.Tus, {
        endpoint: 'https://definitio.div/tus/', // use your tus endpoint here
        resume: true,
        autoRetry: true,
        retryDelays: [0, 1000, 3000, 5000]
      })
    uppy.on('complete', (result) => {
      console.log('Upload complete! We’ve uploaded these files:');
      console.log(result);

      $('#user_image').attr('src', '/storage/uploads/' + result.successful[0].data.name);

      let newUrl = "{{route('ops/filepersist')}}/{{str_replace('@','-at-','aaron.otman@definitio.org')}}";

      console.log(newUrl);
      window.axios.post(newUrl, {
        data: result
      })
        .then(function (response) {
          // currentObj.output = response.data;
          console.log(response);
        })
        .catch(function (error) {
          // currentObj.output = error;
        });

//      let url = "{{route('ops/filepersist')}}/{{str_replace('@','-at-','aaron.otman@definitio.org')}}";

    });


/*
    const Webcam = Uppy.Webcam;
    const Dashboard = Uppy.Dashboard;

    var uppy = Uppy.Core()
      .use(Uppy.Dashboard, {
        inline: true,
        target: '#drag-drop-area',
        note: 'Images and video only, 2–3 files, up to 1 MB',
        width: 260,
        height: 470,
        metaFields: [
          {id: 'name', name: 'Name', placeholder: 'file name'},
          {id: 'caption', name: 'Caption', placeholder: 'describe what the image is about'}
        ],

      })
      .use(Webcam, {target: Dashboard})
      .use(Uppy.Tus, {
        endpoint: 'https://definitio.div/tus/', // use your tus endpoint here
        resume: true,
        autoRetry: true,
        retryDelays: [0, 1000, 3000, 5000]
      })
    uppy.on('complete', (result) => {
      console.log('Upload complete! We’ve uploaded these files:', result.successful);
      let url = "{{route('ops/filepersist')}}/{{str_replace('@', '-at-', $formUser->email)}}";
      $('#user_image').attr('src', '/storage/uploads/'+result.successful[0].data.name);
      window.axios.post(url, {
        data: result
      })
      .then(function (response) {
        // currentObj.output = response.data;
        console.log(response);
      })
      .catch(function (error) {
        // currentObj.output = error;
      });
    });

    */

    @if(!empty($errors->first()))
      bulmaToast.toast({
        message: "The form contained some errors, please revise and resubmit",
        duration: 2000,
        type: "is-danger",
        position: "top-right",
        closeOnClick: true,
        dismissible: true,
        pauseOnHover: true,
        opacity: 1
      });
    @else
      @if(Session::has('message'))
        bulmaToast.toast({
          message: '{{Session::get('message')}}',
          duration: 2000,
          type: "is-success",
          position: "top-right",
          closeOnClick: true,
          dismissible: true,
          pauseOnHover: true,
          opacity: 1
        });
      @endif
    @endif

  </script>
@stop
