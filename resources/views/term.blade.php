@extends('layouts.app')
@section('beef')
  @include('partials.alphabet', array('alphabet' => $alphabet, 'letter'=>$letter, 'goto'=>'alphabet', 'column'=>'initial'))
  <section class="section">
    <div class="container">
      <!-- Load Facebook SDK for JavaScript -->
      <script>
        function share_fb(url) {
          window.open('https://www.facebook.com/sharer/sharer.php?u='+url,'facebook-share-dialog',"width=626, height=436")
        }
      </script>
      <div class="columns is-multiline is-mobile"><s></s>
        <div class="column is-4">
          <a class="button is-medium is-black" href="{{route('term')}}/{{$term->slug}}">{{$term->expression}}</a>
        </div>
        <div class="column">
          @if (false && count($term->related))
            <h5 class="is-inline-block">See also</h5>
          @endif
          <div class="buttons">
            @foreach($term->related as $related)
              <a class="button is-inline is-info is is-outlined" href="{{route('term')}}?q={{$related->expression}}">
                {{$related->expression}}
              </a>
            @endforeach
          </div>
        </div>
      </div>
      <div class="columns snappyline">
        <div class="column is-8  snappyleft">
          @if(null !== $term->time_point)
            <p>{{date('F j, Y, g:i a', strtotime($term->time_point))}}</p>
          @endif
          {!! $srcMarkup !!}
        </div>
        <div class="column snappyright">
          <p>
            @if(\Auth::User())
              <a class="button is-info" href="{{route('define')}}/{{$term->id}}">
                <span class="icon is-small"><i class="fa fa-pen"></i></span>
              </a>
            @endif
            <a class="button"
               onclick="share_fb('{{request()->getUri()}}');return false;"
               rel="nofollow"
               target="_blank"
            >
              <span class="icon is-small"><i class="fab fa-facebook-f"></i></span>
            </a>
            <a class="button">
              <span class="icon is-small">
                <i class="fab fa-twitter"></i>
              </span>
            </a>
            <a class="button" id="printer" target="_blank" href="{{route('publish/html')}}/{{$term->slug}}">
              <span class="icon is-small">
                <i class="fa fa-print"></i>
              </span>
            </a>
            <a class="button" id="pdfmaker" target="_blank" href="{{\Request::getUri()}}.pdf">
              <span class="icon is-small">
                <i class="fa fa-file-pdf"></i>
              </span>
            </a>
            <a class="button">
              <span class="icon is-small">
                <i class="fa fa-envelope"></i>
              </span>
            </a>
          </p>
        </div>
        <div style="clear: both;">

        </div>
      </div>
      <!--div class="columns is-vcentered is-gapless"-->
      <div class="columns is-vcentered is-gapless">
        <div class="column has-text-black">
          {!! $term->content_html!!}
        </div>
      </div>
    </div>
    <div class="container">
      <div class="columns is-vcentered is-gapless">
        <div class="column">
          @foreach($term->definitions as $definition)
            <h5 class="is-size-4">{{$definition->heading}}</h5>
            <h6 class="is-size-6">
              {{$definition->author}}, {{date('F j, Y, g:i a', strtotime($definition->time_point))}}
              , {{$definition->publication}}
            </h6>
            @if (true)
            {!! $definition->content_html !!}
            @endif
          @endforeach
        </div>
      </div>
    </div>
  </section>
  <script>
    @if(!empty($errors->first()))
    bulmaToast.toast({
      message: "The form contained some errors, please revise and resubmit",
      duration: 2000,
      type: "is-danger",
      position: "top-right",
      closeOnClick: true,
      dismissible: true,
      pauseOnHover: true,
      opacity: 1
    });
    @else
    @if(Session::has('message'))
    bulmaToast.toast({
      message: '{{Session::get('message')}}',
      duration: 2000,
      type: "is-success",
      position: "top-right",
      closeOnClick: true,
      dismissible: true,
      pauseOnHover: true,
      opacity: 1
    });
    @endif
    @endif
  </script>
@stop
