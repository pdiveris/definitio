@extends('layouts.app')

@section('beef')
  <section class="section">
    <div class="container">
      <form method="POST" action="{{ route('login') }}">
        <!--form method="POST" action="pako"-->
        @csrf
        <div class="field">
          <p class="control has-icons-left has-icons-right">
            <input
              id="name"
              class="input"
              type="email"
              placeholder="Email"
              name="email"
              value="{{ old('email') }}"
              required
              autofocus
            >
            <span class="icon is-small is-left">
            <i class="fas fa-envelope"></i>
              @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </span>
            <span class="icon is-small is-right">
              <i class="fas fa-check"></i>
            </span>
          </p>
        </div>
        <div class="field">
          <p class="control has-icons-left">
            <input class="input" type="password" placeholder="Password" name="password" required>
            <span class="icon is-small is-left">
              <i class="fas fa-lock"></i>
            </span>
          </p>
        </div>
        <div class="field">
          <p class="control">
            <button type="submit" class="button is-success">
              Login
            </button>
          </p>
        </div>
      </form>
    </div>
  </section>
@endsection
