@extends('layouts.app')
@section('beef')
  <div class="container" id="apostrophe">
    <section class="section">
      <h3 class="is-size-3">{{$term->expression}}</h3>
      <form method="POST" action="{{ \Request::getUri() }}">
        @csrf
        <div class="field is-expanded distraction">
          <input
            class="input subtitle is-info didot"
            value="{{$term->expression}}"
            name="expression"
            type="text"
            onkeydown="$('#help_expression').text('')"
            placeholder="{{$term->expression}}" hidden
          />
          @if($errors->has('expression'))
            <p class="help is-danger" id="help_expression">The field "expression" is required</p>
          @endif
        </div>

        <!-- main text edtor -->
        <div class="field definitions editor_boxed has-dot-separator" id="mother">
          <div
            id="content_edit_container"
            class="editor_trimmed"
            style="outline: rgba(0, 0, 255, 0.3) solid 1px;"
          >
            {!! $term->content_html!!}
          </div>
        </div>
        <div class="is-hidden" style="display: none;">
          <input
            class="is-hidden"
            type="hidden"
            name="content_html"
            id="content_html"
            value="{{ $term->content_html }}"
          />
        </div>
        <div>
          <a href="#" class="didot" onclick="$('#metadata').toggleClass('is-hidden')">Metadata (click to expand)</a>
        </div>
        <div id="metadata" class="is-hidden">
          <!-- pub date -->
          <div class="field distraction">
            <div class="control is-pulled-left" id="content_calendar_container">
              <label class="label is-pulled-left subtitle" style="margin-right: 1em;">
                Published
                @if(!(null == $term->published_at))
                  <input type="checkbox" checked id="tickPublished">
                @else
                  <input type="checkbox" id="tickPublished">
                @endif
              </label>
              <div class="field-body is-pulled-left is-large">
                <div class="field ">
                  <a class="subtitle" href="#" id="toggleCalendar">
                    {{Date('jS M Y', strtotime($term->published_at))}}
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div class="control is-pulled-left distraction" id="published_at">
            <input
              id="publishedAt"
              type="date"
              name="published_at"
              value="{{substr($term->published_at,0,10)}}"
            />
          </div>

          {{--------------------------- META ---------------------------}}
          <div class="field distraction">
            <div class="control">
              <label style="padding-top: 0.75em" class="label subtitle">Primary source</label>
              <label class="label">Label</label>
              <input
                class="input subtitle is-info"
                value="{{$sourceLabel}}"
                type="text"
                name="link_label"
                id="link_label"
                placeholder="Source label"
              />

              <label class="label">URL</label>
              <input
                class="input subtitle is-info"
                value="{{$sourceLink}}"
                type="text"
                name="link_href"
                id="link_href"
                placeholder="Source label"
              />
            </div>
          </div>
          <div class="field distraction">
            <div class="control">
              <label style="padding-top: 0.75em" class="label subtitle">See also</label>
              <input
                class="input tagsinput "
                type="tags"
                placeholder="Add Tag"
                value="{{$related}}"
              />
            </div>
          </div>
          {{--------------------------- /META ---------------------------}}
        </div>

        <div class="is-divider"></div>
          <div class="u-mb-30">
            @foreach($term->definitions as $i=>$definition)
            <h3 class="title is-4">#{{$i+1}}</h3>
            <div class="field is-expanded distraction ">
              <input
                class="input subtitle is-info didot"
                value="{{$definition->heading}}"
                name="definition_heading_{{$definition->id}}"
                type="text"
                onkeydown="$('#help_definition').text('')"
                placeholder="{{$term->expression}}" hidden
              />
              @if($errors->has('definition_heading'))
                <p class="help is-danger" id="help_definition">The title is required</p>
              @endif
            </div>
            <div
              id="definition_edit_container_{{$definition->id}}"
              class="editor_trimmed"
              style="outline: rgba(0, 0, 255, 0.3) solid 1px;"
              >
              {!! $definition->content_html!!}
            </div>
            <div class="is-hidden">
              <input
                class="is-hidden"
                type="hidden"
                name="definition_text_{{$definition->id}}"
                id="definition_text_{{$definition->id}}"
                value="{!! $definition->content_html!!}"
              />
             </div>
            @endforeach
          </div>
        <div class="control distraction">
          <a class="button is-black">Add</a>
        </div>
        <!------------------------------ actions ------------------------------------>
        <div class="is-divider"></div>
        <div class="field" style="margin-top: 1.2em;">
          <button type="submit" class="button is-success">
            Update
          </button>
          <input
            class="is-checkradio is-inline pull-left"
            id="goPreview"
            type="checkbox"
            name="goPreview"
            value="preview"
          />
          <label for="goPreview">Go to term</label>

        </div>
      </form>
      @if($method=='PUT')
        <form method="POST"
              action="{{ $term->deleted_at == null ? route('term/destroy') : route('term/revive')}}/{{$term->id}}">
          @csrf
          @if(null == $term->deleted_at)
            <button type="submit" class="button is-danger is-pulled-right" style="margin-top: -2.4em;">
              Delete
            </button>
          @else
            <button type="submit" class="button is-warning is-pulled-right" style="margin-top: -2.4em;">
              Undelete
            </button>
          @endif
        </form>
      @endif
    </section>
  </div>
  <script>
    let myBody;
    let isFullScreen = false;
    @if(!empty($errors->first()))
    bulmaToast.toast({
      message: "The form contained some errors, please revise and resubmit",
      duration: 2000,
      type: "is-danger",
      position: "top-right",
      closeOnClick: true,
      dismissible: true,
      pauseOnHover: true,
      opacity: 1
    });
    @else
    @if(Session::has('message'))
    bulmaToast.toast({
      message: '{{Session::get('message')}}',
      duration: 2000,
      type: "is-success",
      position: "top-right",
      closeOnClick: true,
      dismissible: true,
      pauseOnHover: true,
      opacity: 1
    });
    @endif
    @endif

    ClassicEditor
      .create(document.querySelector('#content_edit_container'), {
          extracPlugins: 'html5video',
          image: {
            // You need to configure the image toolbar, too, so it uses the new style buttons.
            toolbar: ['imageTextAlternative', '|', 'imageStyle:alignLeft', 'imageStyle:full', 'imageStyle:alignRight'],

            styles: [
              // This option is equal to a situation where no style is applied.
              'full',

              // This represents an image aligned to the left.
              'alignLeft',

              // This represents an image aligned to the right.
              'alignRight'
            ]
          }
        }
      )
      .then(editor => {
        window.editor = editor;

        editor.model.document.on('change:data', (eventInfo, batch) => {
          console.log('Update model..');
          $('#content_html').val(editor.getData());
          // console.log(editor.getData()); // -> '<p>This is editor!</p>'
        });
        // placeholder for future use and reference
        editor.ui.focusTracker.on('change:isFocused', (evt, name, value) => {

        });
        editor.on('FullScreen', () => {
          if (!isFullScreen) {
            // $('#app').hide();

            console.log('FullScreen!');
            let div = $('.editor_boxed');

            $('.ck-content').css('min-height', $(window).height());

            $('nav').hide();
            $('footer').hide();
            $('.section').hide();
            $('.distraction').hide();


            $('.editor_boxed').appendTo('body');
            isFullScreen = true;

          } else {
            // window.location = window.location.pathname;
            $('.editor_boxed').appendTo('#mother');

            $('nav').show();
            $('.distraction').show();
            $('.section').show();

            $('footer').show();

            isFullScreen = false;
            $('.ck-content').css('min-height', 304);
          }
        });
      })
      .catch(error => {
        console.error(error);
      });

    // -----------------------------------------------------------------------------------------------------------
    @foreach($term->definitions as $i=>$definition)
    ClassicEditor
      .create(document.querySelector('#definition_edit_container_{{$definition->id}}'), {
        extracPlugins: 'html5video',
          image: {
            toolbar: ['imageTextAlternative', '|', 'imageStyle:alignLeft', 'imageStyle:full', 'imageStyle:alignRight'],
            styles: ['full', 'alignLeft', 'alignRight']
          }
        }
      )
      .then(editor => {
        window.editor = editor;
        editor.model.document.on('change:data', (eventInfo, batch) => {
          $('#definition_text_{{$definition->id}}').val(editor.getData());
        });
        editor.ui.focusTracker.on('change:isFocused', (evt, name, value) => {
        });
      })
      .catch(error => {
        console.error(error);
      });
    // -----------------------------------------------------------------------------------------------------------
      @endforeach

    let l = ClassicEditor.builtinPlugins.map(plugin => plugin.pluginName);
    console.log(l);

    // Initialize all input of date type.
    var calendars = bulmaCalendar.attach('[type="date"]', {
      displayMode: 'inline',
      closeOnSelect: true,
      showHeader: false,
      dateFormat: 'YYYY-MM-DDTHH:mm:ss'
    });

    var tagsinput = bulmaTagsinput.attach('[type="tags"]');

    // Loop on each calendar initialized
    for (var i = 0; i < calendars.length; i++) {
      // Add listener to date:selected event
      calendars[i].on('date:selected', date => {
        $('#published_at').addClass('is-hidden');
        console.log(date);
        $('#toggleCalendar').text(moment(date.start).format('MMM Do YYYY'));

        $('#tickPublished').prop('checked', true);

        console.log(date.start);
      });
    }

    $('#toggleCalendar').on('click', function () {
      $('#published_at').toggleClass('is-hidden');
    });

    $('#tickPublished').on('click', function (e) {
      // $('#published_at').toggleClass('is-hidden');
      if ($('#tickPublished').prop('checked') === false) {
        $('#toggleCalendar').text('');
        $('#published_at').addClass('is-hidden');
      } else {
        $('#published_at').toggleClass('is-hidden');
      }
    });
    $('#fullScreen').click(function (event) {
        event.preventDefault();

        const div = $("<div>");
        div.width("100%");
        div.height("100%");
        const mother = $('#mother');

        mother.width("100%");
        mother.height("100%");
        $('.ck-editor__editable').height('1000px');

        div.append(mother);
        $('#app').hide();
        $("body").prepend(div);
        //div.prepend($('#content_edit_container'));
      }
    );
  </script>
@stop
