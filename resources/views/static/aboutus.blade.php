@extends('layouts.app')
@section('beef')
<section class="section">
  <div class="container">
    <div class="columns">
      <div class="column">
        <a class="button is-black is-medium is-rounded is-danger" href="/" >/</a>
        @foreach($alphabet as $alpha)
          @if(strtolower($alpha->initial) == strtolower($letter))
            <a class="button is-black is-medium" href="{{route('entries')}}?q={{$alpha->initial}}">
          @else
            <a class="button is-outlined is-medium" href="{{route('entries')}}?q={{$alpha->initial}}">
          @endif
            {{$alpha->initial}}
            </a>
         @endforeach
      </div>
    </div>

  </div>
</section>

<section class="section">
  <div class="container">
    <div class="columns">
      <div class="column">
        <h2 class="title">About us</h2>
        <h3 class="subtitle">We, the undersigned..</h3>
        @foreach(\App\Editor::orderBy('name')->get() as $i=>$editor)
          <div class="box">
            <article class="media">
              <div class="media-left">
                <figure class="image is-64x64">
                  <!-- Show image at 200px -->
                  <img src="{{ \App\Helpers\Utils::gravata($editor->email, 64) }}">
                </figure>
              </div>
              <div class="media-content">
                <div class="content">
                  <p>
                    <a
                      class="button is-medium is-inline-block-desktop"
                      href="{{route('term')}}?q={{$editor->name}}"
                      style="margin-top: -10px;">
                      {{$editor->name}}

                    </a>
                    <small>@ {{ $editor->handle }}</small>

                  </p>
                </div>
              </div>
            </article>
          </div>
        @endforeach
{{--
        <ul>
          @foreach(\App\Editor::orderBy('name')->get() as $i=>$editor)
            <ul>{{$editor->name}}</ul>
          @endforeach
        </ul>
--}}

      </div>
    </div>
  </div>
</section>
@stop
