@extends('layouts.app')
@section('beef')

  <section class="section">
    <h2>{{$host}}/tus</h2>
    <figure class="media-left">
      <p class="image is-256x256">
        <img style="width: 256px;" src="{{ \App\Helpers\Utils::gravata('aaron.otman@definitio.org', 256) }}"
             id="user_image">
      </p>
    </figure>
    <div>
      <div id="drag-drop-area"></div>
    </div>

    {{--    <div class="container">
          <div class="columns">
            <div class="column is-10">
              <div class="timeline">
                <header class="timeline-header">
                  <span class="tag is-medium is-primary">Start</span>
                </header>

                <div class="timeline-item">
                  <div class="timeline-marker is-image is-32x32">
                    <img src="http://bulma.io/images/placeholders/32x32.png">
                  </div>

                  <div class="timeline-content">
                    <p class="heading">
                      Definition
                    </p>
                    <p>
                    <div>
                      Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                      printer took a galley of type and scrambled it to make a type specimen book.
                      It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                      essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                      containing Lorem Ipsum passages, and more recently with desktop publishing software like
                      Aldus PageMaker including versions of Lorem Ipsum.
                    </div>
                    </p>
                  </div>
                </div>

                <div class="timeline-item">
                  <div class="timeline-marker is-image is-32x32">
                    <img src="http://bulma.io/images/placeholders/32x32.png">
                  </div>
                  <div class="timeline-content">
                    <p class="heading">
                      Petros Diveris, Feb. 27
                    </p>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce malesuada velit ultricies purus porta
                      ultricies. Donec eu magna maximus, viverra enim at, viverra leo. Fusce dignissim elit at turpis
                      bibendum auctor. Praesent placerat, neque ac vestibulum ultricies, augue nisi hendrerit neque,
                      blandit vestibulum lectus tellus vitae massa. Donec varius quam ac dolor cursus aliquam.
                      Quisque tellus ligula, elementum egestas dignissim vitae, gravida vitae nisi.
                    </p>
                  </div>
                </div>
                <div class="timeline-header">
                  <span class="tag is-medium is-primary">End</span>
                </div>
              </div>


            </div>

            <div class="column">
              <p>Kolumn #2</p>
              <div class="is-divider" data-content="OR"></div>
              <p>Nice work</p>
            </div>
          </div>
        </div>--}}
  </section>
<script>
  const Webcam = Uppy.Webcam;
  const Dashboard = Uppy.Dashboard;

  var uppy = Uppy.Core()
    .use(Uppy.Dashboard, {
      inline: true,
      target: '#drag-drop-area',
      note: 'Images and video only, 2–3 files, up to 1 MB',
      width: 260,
      height: 470,
      metaFields: [
        {id: 'name', name: 'Name', placeholder: 'file name'},
        {id: 'caption', name: 'Caption', placeholder: 'describe what the image is about'}
      ],

    })
    .use(Webcam, {target: Dashboard})
    .use(Uppy.Tus, {
      endpoint: 'https://definitio.div/tus/', // use your tus endpoint here
      resume: true,
      autoRetry: true,
      retryDelays: [0, 1000, 3000, 5000]
    })
  uppy.on('complete', (result) => {
    console.log('Upload complete! We’ve uploaded these files:');
    console.log(result.successful[0].name);
    $('#user_image').attr('src', '/storage/uploads/' + result.successful[0].data.name);
    // alert('/storage/uploads/' + result.successful[0]['name']);

  });

</script>
@stop
