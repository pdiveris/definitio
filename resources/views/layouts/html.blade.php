<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>de-font</title>

  <script
    src="https://code.jquery.com/jquery-3.3.1.slim.js"
    integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
    crossorigin="anonymous"></script>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=GFS+Didot&amp;subset=greek" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css"
    integrity="sha256-dMQYvN6BU9M4mHK94P22cZ4dPGTSGOVP41yVXvXatws="
    crossorigin="anonymous"
  />

  <!-- Styles -->
  <style>
    html, body {
      background-color: #fff;
      color: #636b6f;
      font-weight: 200;
      height: 100vh;
      margin: 0;
    }

    /*
        Based on the work and writing of Laura Franz, UMass Daertmouth
        https://www.smashingmagazine.com/2012/04/drop-caps-historical-use-and-current-best-practices/

        Also, look at how they've been implemented at:

        [Aeon](https://aeon.co/) and
        [The Guardian](https://www.theguardian.com/uk)

        The Times spans three lines which I think is nicer. Hm.
        https://www.definitio.org/caps/thetimes/A.png

        Capsnaps also available here:
        https://www.definitio.org/caps/

    */
    .outercap {
      float: left;
      width: 70px;
      height: 56px;
      margin-top: 0px;
      padding-top: 20px;
      margin-right: 8px;
      text-align: center;
      background: url(/images/box_white_1x1.png) 100% 100%;
    }

    .innercap {
      font-size: 3.5em;
      color: #000000;
      margin-top: -43px;
      margin-left: 7px;

    }

    .didot {
      font-family: 'GFS Didot', serif;
    }

    .lato {
      font-family: 'Lato', sans-serif;
    }
  </style>

  <script>
  </script>
</head>
<body>
<div class="control">
  <label class="radio">
    <input type="radio" value="didot" checked name="font">
    Didot
  </label>
  <label class="radio">
    <input type="radio" value="lato" name="font">
    Lato
  </label>
</div>
@yield('beef')
<footer class="footer">
  <div class="content has-text-centered">
    <p>
      <strong><span class="is-danger">d</span></strong><strong>efinitio</strong> brought to you by <a
        href="https://www.diveris.org">Petros Diveris</a>.

      <span class="subtitle">D: 0.3</span>
      <span class="subtitle">L: {{app()->version()}}</span>
      <br/>
      <span class="subtitle">All content Creative Commons. All code is <a href="#">GPL</a></span>
    </p>
    </p>
  </div>
</footer>
</div>

<script type="text/javascript">
  $(document).ready(function () {

    $('input:radio[name="font"]').change(function () {
      if ($(this).val() == 'didot') {
        $('.container').removeClass('lato').addClass('didot');
      }
      if ($(this).val() == 'lato') {
        $('.container').removeClass('didot').addClass('lato');
      }

    });

  });
</script>
</body>
</html>
