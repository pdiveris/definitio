<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/html"
      xmlns="http://www.w3.org/1999/html">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>definitio - {{$term->expression}}</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=GFS+Didot&amp;subset=greek" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

  <!-- main css -->
  <link rel="stylesheet" type="text/css" href="{{secure_asset('css/app.css')}}">

  <!-- Bulma extras -->
  <link rel="stylesheet" type="text/css" href="{{secure_asset('bulma-divider/css/bulma-divider.min.css')}}">

  <!-- app -->
  <script src="{{secure_asset('js/app.js')}}"></script>

  <!-- Styles -->
  <style>

  </style>
  <script>
    $(function () {
      $('html').addClass('dom-loaded');
      console.log('Print engine 0.1.0');

    });
  </script>
</head>
<body>
@yield('beef')
<footer class="footer">
  <div class="content has-text-centered">
    <p>
      <strong><span class="is-danger">d</span></strong><strong>efinitio</strong> brought to you by <a href="https://www.diveris.org">Petros Diveris</a>.
      <span class="subtitle">D: 0.3</span>
      <span class="subtitle">L: {{app()->version()}}</span>
      <br/>
      <span class="subtitle">All content Creative Commons. All code is <a href="#">GPL</a></span>
    </p>
  </div>
</footer>
</div>

<!-- Matomo -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//metrics.definitio.org/metrics/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Matomo Code -->
</body>
</html>
