<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {!! \App\Http\Controllers\DefinitioSiteController::getMetaMarkup() !!}
  <title>definitio
    @if (isset($term) && null !== $term)- {{$term->expression}}@endif</title>
  <link rel="stylesheet" type="text/css" href="{{secure_asset('css/app.css')}}">
  <link rel="stylesheet" type="text/css" href="{{secure_asset('css/definitio.css')}}">
  @if (isset($term) && null !== $term)
  <link rel="alternate" href="{{\Request()->getUri()}}.xml" title="RSS" type="application/rss+xml">
  @endif
  <!-- Bulma extras -->
  <link rel="stylesheet" type="text/css" href="{{secure_asset('bulma/calendar/bulma-calendar.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{secure_asset('bulma-tagsinput/dist/css/bulma-tagsinput.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{secure_asset('bulma-divider/css/bulma-divider.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{secure_asset('bulma-timeline/dist/css/bulma-timeline.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{secure_asset('bulma-switch/dist/css/bulma-switch.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{secure_asset('bulma-checkradio/dist/css/bulma-checkradio.min.css')}}">

  <!-- app -->
  <script src="{{secure_asset('js/app.js')}}"></script>

  <!-- Bulma and other UI extensions -->
  <script src="{{secure_asset('bulma/calendar/bulma-calendar.min.js')}}"></script>
  <script src="{{secure_asset('moment/moment.min.js')}}"></script>

  <script src="{{secure_asset('bulma-tagsinput/dist/js/bulma-tagsinput.min.js')}}"></script>
  <script src="{{secure_asset('bulma/toast/bulma-toast.min.js')}}"></script>
  <!-- CKEditor -->
  <script src="{{secure_asset('ckeditor/definitio/classic/ckeditor.js')}}"></script>
  <link rel="stylesheet" href="{{secure_asset('ckeditor/definitio/classic/styles.css')}}" >

  @if(Request::header('wkid')=='snapper')
  <link rel="stylesheet" type="text/css" href="{{secure_asset('css/snapper.css')}}">
  <!-- cash DOM rocks -->
  <script src="{{asset('/cash/2.3.7/cash.min.js')}}"></script>
  @endif

@if(isset($uploader))
    @if($uploader == 'uppy')
    <!-- uppy.js -->
    <link href="{{asset('uppy/uppy.min.css')}}" rel="stylesheet">
    <script src="{{secure_asset('uppy/uppy.min.js')}}"></script>
    @endif
    @if($uploader == 'filepond')
    <!-- filepond.js -->
    <link rel="stylesheet" type="text/css" href="{{secure_asset('filepond/filepond.min.css')}}">
    <script src="{{secure_asset('filepond/filepond.min.js')}}"></script>
    @endif

 @endif
  <script>
    $(function () {
      $('html').addClass('dom-loaded');
      $('.new').attr('target', '_blank');
      $('.toggler').on('click', function () {
        $('.expander').toggleClass('is-hidden');

        if ($('.toggler').text() === 'Read more +') {
          $('.toggler').text('Collapse -');
        } else {
          $('.toggler').text('Read more +');
        }
      });
    });
  </script>
</head>
<body>
<div id="app">
  <div id="kontainer">
    <nav class="navbar" id="bara">
      <div class="container">
        <div class="navbar-burger burger"
             onclick="document.querySelector('.navbar-menu').classList.toggle('is-active');" data-target="navMenu">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div id="navMenu" class="navbar-menu">
          <div class="navbar avant" id="bara2">
            @if (Auth::check())
              @if(Auth::user()->hasRole('administrator'))
                <a href="{{route('backstage/home')}}" class="navbar-item">
                  <i style="margin-top: 7px;" class="fas fa-cogs fa-lg"></i>
                </a>
              @endif
              <a href="{{route('logout')}}" class="navbar-item">Logout</a>
              <a href="{{route('terms')}}" class="navbar-item">Terms</a>
              <a href="{{route('tags')}}" class="navbar-item">Tags</a>
            @else
              <a href="{{route('login')}}" class="navbar-item">Login</a>
              <a href="{{route('suggest')}}" class="navbar-item">Suggest</a>
              <a href="{{route('become')}}" class="navbar-item">Become an editor</a>
            @endif
            <a href="{{route('about-us')}}" class="navbar-item">About</a>
            <a href="{{route('privacy')}}" class="navbar-item">Privacy</a>
          </div>
          <div id="barEnd" class="navbar-end">
            <a id="barItem" class="navbar-item" href="/" style="font-weight:bold;">
              <img id="logoImage"
                   src="https://www.definitio.org/assets/img/logo3.png"
                   alt="Definitio: seeing through the many euphemisms"
              >
            </a>
          </div>
        </div>
      </div>
    </nav>
    @yield('beef')
    <footer class="footer">
      <div class="content has-text-centered">
        <p>
          <strong><span class="is-danger">d</span></strong><strong>efinitio</strong> brought to you by <a
            href="https://www.diveris.org">Petros Diveris</a>.
          <br/>
          <span class="title-6">R: {{\App\Helpers\Utils::getRevisionString()}}</span>
          <span class="title-6">L: {{app()->version()}}</span>
          <br/>
          <span class="subtitle">All Content Creative Commons; Code is <a href="#">GPL</a></span>
          |
          <span class="subtitle"><a href="{{route('become')}}">Privacy policy</a></span>
        </p>
      </div>
    </footer>
  </div>

</div>
<!-- Matomo -->
<script>
  function snapper() {
    console.log("Webkit..");

    x = $('#navMenu');
    x.removeClass('navbar-menu');
    x.addClass('snapper');
   }

  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function () {
    var u = "//metrics.definitio.org/metrics/";
    _paq.push(['setTrackerUrl', u + 'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
    g.type = 'text/javascript';
    g.async = true;
    g.defer = true;
    g.src = u + 'piwik.js';
    s.parentNode.insertBefore(g, s);
  })();
</script>
<!-- End Matomo Code -->
</body>
</html>
