@extends('layouts.book')
@section('beef')

  <section class="section">
    <div class="container didot">
      <div class="columns">
        <div class="column">
          <div class="content is-medium">
            <h1 class="didot">{{$term->expression}}</h1>
            {!! $term->content_html !!}
            @foreach($term->definitions as $definition)
              <h5 class="is-size-4">{{$definition->heading}}</h5>
              <h6 class="is-size-6">
                {{$definition->author}}, {{date('F j, Y, g:i a', strtotime($definition->time_point))}}
                , {{$definition->publication}}
              </h6>
              <div class="didot u-mb-30">
              {!! $definition->content_html!!}
              </div>
            @endforeach

          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container  has-text-centered u-mbottom-30 u-mtop-30">
      <div class="columns is-centered">
        <div class="column is-2">
          <img style="width: 40px;" src="{{asset("ornaments/leaf.jpg")}}" />
{{--
          <img style="width: 40px;" src="https://www.definitio.org/ornaments/leaf.jpg" />
--}}
        </div>
      </div>
    </div>
  </section>
@stop
