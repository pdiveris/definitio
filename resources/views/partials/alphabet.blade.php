<section class="section">
  <div class="container">
    <div class="columns">
      <div class="column">
        <a class="button is-black is-medium is-rounded is-danger" href="{{route($goto)}}" >/</a>
        @foreach($alphabet as $alpha)
          @if(strtolower($alpha->$column) == strtolower($letter))
            <a class="button is-black u-m-1 is-medium" href="{{route($goto)}}?q={{$alpha->$column}}">
          @else
            <a class="button is-outlined u-m-1 is-medium" href="{{route($goto)}}?q={{$alpha->$column}}">
          @endif
             {{$alpha->$column}}
            </a>
         @endforeach
      </div>
    </div>
  </div>
</section>
