@extends('layouts.html')
@section('beef')
  <section class="section">
    <div class="container didot">

      <div class="content is-medium">
        <h1>Wetherspoons</h1>
        <p>
        <div class="outercap">
          <div class="innercap">
            W
          </div>
        </div>
        etherspoons, as any fool know, is an enormous chain of enormous cut-price pubs.
        At any of the 900 or so
        branches of ‘Spoons, you can get a pint and a meal in an often beautiful setting at a surprisingly
        affordable price. You can even, thanks to its app, get the pub’s hard-pressed staff to bring you said
        affordable pint and meal, without ever getting off your fat arse, which is ideal for the drinking millennial,
        if not for the actual staff. We’ve got no problem with Wetherspoons as a pub company:
        Wetherspoons is, basically, good.
        </p>
        <p>
        <div class="outercap">
          <div class="innercap">
            Ω
          </div>
        </div>

          ραιόκαστρο αποτελεί τον παλαιότερο οικισμό, που βρίσκεται νότια του Ωραιοκάστρου. Ο ναός του Αγίου Αθανασίου
          Παλαιοκάστρου ανάγεται πριν το 1864 καθώς επιγραφή διασώζει το γεγονός της ανακαίνισης του ναού μετά από
          πυρκαγιά το έτος 1864. Στην τοιχοποιία του αποκαλύφθηκαν μαρμάρινες επιγραφές ελληνιστικών χρόνων, τμήματα
          κιονοκράνων και μαρμάρων από αρχαία κτίσματα.
        </p>
        <p>
          The spectacle before us was indeed sublime.
        </p>
        <p>
        <div class="outercap">
          <div class="innercap">
            S
          </div>
        </div>
        omething that is not good, however, is the fact that an affordable pint and meal is not the only thing on
        offer in the average Wetherspoons pub. Lying about the place, between an entirely extraneous layer of crumbs and
        a
        pool of spilt beer, you will find copies of <em> <a href="https://bit.ly/2T8Etpp">Wetherspoons News</a></em>,
        the chain’s in-house-magazine-cum-hard-Brexit-propaganda-sheet. It’s always been a deeply
        weird publication, occupying a strange hinterland between the Ukip manifesto and the Camra guide to
        Berkshire and Surrey.
        </p>
        <p>
        <div class="outercap">
          <div class="innercap">
            B
          </div>
        </div>
        ut now it turns out it’s gone a step beyond weird and into the realms of completely fucking doolally. Let’s, at
        this juncture, turn to <a
          href="https://www.buzzfeed.com/markdistefano/wetherspoons-hard-brexit-magazine-spectator-columns?utm_source=dynamic&amp;utm_campaign=bfsharetwitter&amp;utm_term=.qj8QQ5gNJJ"><em>Buzzfeed</em>’s
          Mark Di Stefano</a>, who I am particularly keen to credit because the <em>New Statesman</em>, unlike certain
        other publications I’m about to name, prides itself on not simply ripping off other journalists’ work:
        </p>
        <p>
          Let’s say that again. Tim Martin – ale aficionado, pub entrepreneur, enthusiast for the hardest of hard
          Brexits –
          owns a magazine which is simply lifting other publications work, and republishing it.
        </p>
        <p>
        <div class="outercap">
          <div class="innercap">
            O
          </div>
        </div>
        ne of the pieces that Wetherspoons News has “borrowed”, as it turns out, was written for the very website you
        are
        reading right now, by our much missed former colleague Amelia Tait. And so, in the name of harmony and
        friendship,
        we at the New Statesman would like to make Tim Martin an offer:
        </p>
      </div>
    </div>
  </section>


@stop
