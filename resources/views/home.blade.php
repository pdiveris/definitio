@extends('layouts.app')
@section('beef')
  @include('partials.alphabet', array('alphabet' => $alphabet, 'letter'=>$letter, 'goto'=>'alphabet', 'column'=>'initial'))
  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <span class="start_tags"></span>
          @foreach($items as $item)
            <a class="button is-medium u-m-1 is-inline-block-desktop"
               href="{{route('term')}}/{{$item->slug}}">{{$item->expression}}</a>
          @endforeach
          <span class="end_tags"></span>
        </div>
      </div>
    </div>
  </section>
@stop
