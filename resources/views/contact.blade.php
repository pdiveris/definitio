@extends('layouts.app')
@section('beef')
<section class="section">
  <div class="container">
    <form method="POST" action="{{ route('login') }}">
      <!--form method="POST" action="pako"-->
      @csrf
      <div class="field">
        <label class="label">Your name *</label>
        <div class="control has-icons-left has-icons-right">
          <input
            class="input is-success" type="text"
            placeholder="Your name"
            name="name"
            id="name"
            required
            autofocus
          >
          <span class="icon is-small is-left">
          <i class="fas fa-user"></i>
        </span>
          <span class="icon is-small is-right">
          <i class="fas fa-check"></i>
        </span>
        </div>
      </div>

      <div class="field">
        <label class="label">Your email *</label>
        <div class="control has-icons-left has-icons-right">
          <input
            class="input" type="email"
            placeholder="Email" name="email" value="{{ old('email') }}"
            required
          >
          <span class="icon is-small is-left">
            <i class="fas fa-envelope"></i>
            @if ($errors->has('email'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            </span>
          <span class="icon is-small is-right">
              <i class="fas fa-check"></i>
            </span>
        </div>
      </div>

    <div class="field">
      <label class="label">Subject {{$request}}</label>
      <div class="control">
        <div class="select">
          <select>
            <option>Select dropdown</option>
            <option @if($request === 'suggest') {{'selected'}} @endif>Can I suggest a term please?</option>
            <option  @if($request === 'become') {{'selected'}} @endif>Can I be an editor please?</option>
          </select>
        </div>
      </div>
    </div>

    <div class="field">
      <label class="label">Message</label>
      <div class="control">
        <textarea class="textarea" placeholder="Textarea"></textarea>
      </div>
    </div>

    <div class="field">
      <div class="control">
        <label class="checkbox">
          <input type="checkbox">
          I agree to the <a href="#">terms and conditions</a>
        </label>
      </div>
    </div>

{{--
    <div class="field">
      <div class="control">
        <label class="radio">
          <input type="radio" name="question">
          Yes
        </label>
        <label class="radio">
          <input type="radio" name="question">
          No
        </label>
      </div>
    </div>
--}}

    <div class="field is-grouped">
      <div class="control">
        <button class="button is-link">Submit</button>
      </div>
      <div class="control">
        <button class="button is-text">Cancel</button>
      </div>
    </div>
  </div>
</section>

@stop
