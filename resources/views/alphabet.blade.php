@extends('layouts.app')
@section('beef')
  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <a class="button is-black is-medium is-rounded is-danger" href="/">/</a>
          @foreach($alphabet as $alpha)
            @if(strtolower($alpha->initial) == strtolower($letter))
              <a class="button is-black is-medium" href="{{route('entries')}}?q={{$alpha->initial}}">
                @else
                  <a class="button is-outlined is-medium" href="{{route('entries')}}?q={{$alpha->initial}}">
                    @endif
                    {{$alpha->initial}}
                  </a>
              @endforeach
        </div>
      </div>
    </div>
  </section>
  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <span class="start_tags"></span>
          @foreach($items as $item)
            <a class="button is-medium is-inline-block-desktop"
               href="{{route('term')}}?q={{$item->expression}}">{{$item->expression}}</a>
          @endforeach
          <span class="end_tags"></span>
        </div>
      </div>
    </div>
  </section>
@stop
