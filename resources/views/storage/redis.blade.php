@extends('layouts.app')
@section('beef')
  @csrf
  <section class="section">
    <div class="container">
      <h2 class="avant title is-3">redis</h2>
      <!-- system actions, i.e. flushall -->
        <div class="columns">
          <div class="column">
            <div>
              @if($dbId==-1)
                <a href="{{route('backstage/redis')}}?command=flushdb" class="button is-danger">flushdb</a>
              @else
                <a href="{{route('backstage/redis')}}/{{$dbId}}?command=flushdb" class="button is-danger">flushdb</a>
              @endif
              <a href="{{route('backstage/redis')}}/{{$dbId}}?command=addrandom" class="button is-outlined">random</a>
            </div>
          </div>
        </div>

          <form action="{{\Request::getUri()}}" method="post">
            @csrf
            <div class="columns">
              <div class="column is-4">
                <div class="control ">
                  <input
                    class="input is-info didot"
                    type="text"
                    value=""
                    name="key"
                    id="key"
                    placeholder="key"
                  />
                  <input
                    type="hidden"
                    class="is-hidden"
                    value="{{$dbId}}"
                    name="dbId"
                  />
                  <input
                    type="hidden"
                    class="is-hidden"
                    value="-1"
                    name="needle"
                  />
                </div>
              </div>
              <div class="column is-5">
                <div class="control ">
                  <input
                    class="input is-info didot"
                    type="text"
                    value=""
                    name="value"
                    id="value"
                    placeholder="value"
                  />
                </div>
              </div>
              <div class="column is-1">
                <div class="control ">
                  <input
                    class="input is-info didot"
                    type="text"
                    value="3600"
                    name="ttl"
                    id="ttl"
                    placeholder="ttl"
                  />
                </div>
              </div>
              <div class="column is-2">
                <button
                  type="submit"
                  style="float: left"
                  class="button is-info inline-flex"
                  name="add"
                  value="Add"
                >Add
                </button>
              </div>

            </div>
          </form>

        <div class="is-divider"></div>

      <!-- databases -->
        <div class="columns">
          <div class="column">
            <a href="{{route('backstage/redis')}}" class="button {{$dbId == -1 ? 'is-black' : 'is-outlined' }}">all</a>
            @foreach($databases as $tag=>$db)
              <a href="{{route('backstage/redis', [$db])}}"
                 class="button {{$dbId == $db ? 'is-black' : 'is-outlined' }}"
                >
                {{$tag}}
              </a>
            @endforeach
          </div>
        </div>

        <!-- items -->
        <div class="columns">
          <div class="column">

              @foreach ($dump as $key=>$object)
              <form action="{{\Request::getUri()}}" method="post">
                @csrf
                <div class="u-mb-10">
                  <input
                    type="hidden"
                    class="is-hidden"
                    value="{{$dbId}}"
                    name="dbId"
                  />
                  <input
                    type="hidden"
                    class="is-hidden"
                    value="{{$key}}"
                    name="needle"
                  />
                  <button type="submit"
                    class="button is-small is-danger"
                    name="delete"
                    value="del"
                  >del
                  </button>
                  <div style="display: inline-block; min-width: 60px; text-align: right; margin-right: 8px;">
                    <h3 style="display: inline" class="has-text-black">{{$object['ttl']}}</h3>
                  </div>
                  <div style="display: inline;">
                    <h3 style="display: inline">{{$key}}</h3>
                  </div>
                </div>
              </form>
              @endforeach
          </div>
        </div>
    </div>
  </section>
  <script>
    @if(Session::has('error'))
      bulmaToast.toast({
        message: '{{Session::get('error')}}',
        duration: 3500,
        type: "is-danger",
        position: "top-right",
        closeOnClick: true,
        dismissible: true,
        pauseOnHover: true,
        opacity: 1
      });
    @endif
    @if(Session::has('message'))
      bulmaToast.toast({
        message: '{{Session::get('message')}}',
        duration: 2000,
        type: "is-success",
        position: "top-right",
        closeOnClick: true,
        dismissible: true,
        pauseOnHover: true,
        opacity: 1
      });
    @endif
  </script>
@stop
