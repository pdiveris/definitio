@extends('layouts.app')
@section('beef')
    @include('partials.alphabet', array('alphabet' => $alphabet, 'letter'=>$letter, 'goto'=>'terms', 'column'=>'initial'))
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column">
                    @if(false)
                        @foreach($items as $item)
                            <p>
                                <a class="button is-medium is-inline-block-desktop" href="{{route('term')}}?q={{$item->expression}}">{{$item->expression}}</a>
                            </p>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
@stop
