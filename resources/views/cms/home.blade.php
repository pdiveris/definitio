@extends('layouts.app')
@section('beef')
{{--
@include('partials.alphabet', array('alphabet' => $alphabet, 'letter'=>$letter, 'goto'=>'terms', 'column'=>'initial'))
--}}

  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <span class="title is-4">dashboard</span>
          <ul class="didot u-mtop-15">
            <li>
              <a href="{{route('backstage/users')}}" class="navbar-item">Users</a>
            </li>
            <li>
              <a href="{{route('send')}}/MyTtitle/MyContent" class="navbar-item">Mailshot test</a>
            </li>
            <li>
              <a href="{{route('stream/publish')}}" class="navbar-item">Stream Publish /</a>
            </li>
            <li>
              <a href="{{route('stream/client')}}" class="navbar-item">Stream client</a>
            </li>
            <li>
              <a href="{{route('tristram')}}" class="navbar-item">Wired editor (Tristram)</a>
            </li>
            <li>
              <a href="{{route('tristram/mde')}}" class="navbar-item">MDE</a>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </section>

@stop
