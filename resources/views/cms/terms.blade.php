@extends('layouts.app')
@section('beef')
    @include('partials.alphabet', array('alphabet' => $alphabet, 'letter'=>$letter, 'goto'=>'terms', 'column'=>'initial'))
    <section class="section didot">
        <div class="container">
            <div class="columns is-gapless">
                <div class="column">
                    <a class="button is-medium is-pulled-right is-info is-inline-block-desktop" href="{{route('terms.create')}}/">
                        <i class="fas fa-file"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="columns">
                <div class="column">
                    @foreach($terms as $term)
                        <p class="pushdown">
                        @if(null !== $term->deleted_at)
                            <form  method="POST" action="{{ route('terms.revive') }}/{{$term->id}}">
                                @if(null !== Auth::user() && Auth::user()->userHasAdminAccessToTerm($term) )
                                    <a class="button is-medium is-inline" href="{{route('terms.edit')}}/{{$term->id}}">{{$term->expression}}</a>
                                @else
                                    <a class="button is-medium is-inline" href="{{route('terms.edit')}}/{{$term->id}}">{{$term->expression}}</a>
                                @endif
                                @if(null !== Auth::user() && Auth::user()->userHasAdminAccessToTerm($term) )
                                    <button type="submit" class="unstyled-button">
                                        <i class="fas fa-undo fa-2x" style="margin-top: 0.14em"></i>
                                    </button>
                                @endif
                                @csrf
                            </form>
                        @else
                            <form method="POST" action="{{ route('terms.destroy') }}/{{$term->id}}">
                                <input type="hidden" name="_method" value="delete" />
                                @if(null !== Auth::user() && Auth::user()->userHasAdminAccessToTerm($term) )
                                    <a class="button is-medium is-inline" href="{{route('terms.edit')}}/{{$term->id}}">{{$term->expression}}</a>
                                @else
                                    <a class="button is-medium is-inline disabled" href="{{route('terms.edit')}}/{{$term->id}}">{{$term->expression}}</a>
                                @endif
                                @csrf
                                @if(null !== Auth::user() && Auth::user()->userHasAdminAccessToTerm($term))
                                    <button type="submit" class="unstyled-button">
                                        <i class="fas fa-trash fa-2x" style="margin-top: 0.14em"></i>
                                    </button>
                                @endif
                            </form>
                            @endif
                            </p>
                            @endforeach
                </div>
            </div>
        </div>
    </section>
    <script>
        @if(Session::has('message'))
            bulmaToast.toast({
                message: '{{Session::get('message')}}',
                duration: 2000,
                type: "is-success",
                position: "top-right",
                closeOnClick: true,
                dismissible: true,
                pauseOnHover: true,
                opacity: 1
            });
        @endif
    </script>
@stop
