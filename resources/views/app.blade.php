<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    {!! \App\Http\Controllers\DefinitioPublishingController::getMetaMarkup() !!}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <script src="{{ mix('/js/app.js') }}" defer></script>
    <script type="text/javascript" src="/bulma-quickview/js/bulma-quickview.min.js"></script>
    @routes
</head>
<body>
@inertia
<script>
    @if(!empty($errors->first()))
    bulmaToast.toast({
        message: "The form contained some errors, please revise and resubmit",
        duration: 2000,
        type: "is-danger",
        position: "top-right",
        closeOnClick: true,
        dismissible: true,
        pauseOnHover: true,
        opacity: 1
    });
    @else
    @if(Session::has('message'))
    bulmaToast.toast({
        message: '{{Session::get('message')}}',
        duration: 2000,
        type: "is-success",
        position: "top-right",
        closeOnClick: true,
        dismissible: true,
        pauseOnHover: true,
        opacity: 1
    });
    @endif
    @endif
    var quickviews = bulmaQuickview.attach(); // quickviews now contains an array of all Quickview instances
</script>
</body>
</html>
