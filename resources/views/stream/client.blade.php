@extends('layouts.app')
@section('beef')
  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <h2 class="title is-2">Tristram Client</h2>

        </div>
      </div>
    </div>
  </section>
  <script>
/*

    const es = new EventSource('{{$rootUrl}}/mercure/hub?topic=' + encodeURIComponent('http://example.com/books/1'));
    es.onmessage = e => {
      // Will be called every time an update is published by the server
      console.log(JSON.parse(e.data));
    }
*/


    const url = new URL('{{$rootUrl}}/mercure/hub');
    url.searchParams.append('topic', '{{$rootUrl}}/mercure/demo/books/{id}');
    url.searchParams.append('topic', '{{$rootUrl}}/users/dunglas');

    const eventSource = new EventSource(url);

    // The callback will be called every time an update is published
    eventSource.onmessage = e => console.log(e); // do something with the payload

  </script>
@stop
