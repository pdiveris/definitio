@extends('layouts.backstage')
@section('beef')
  <section class="section">
    <div class="container">
      <div class="columns">
        <div class="column">
          <h2 class="title is-2">Tristram Editor</h2>
          <textarea id="tristram"></textarea>
          <h3 id="lastEventId">&nbsp;</h3>
        </div>
      </div>
    </div>
  </section>
  <script>

    const url = new URL('{{$rootUrl}}/mercure/hub');
    url.searchParams.append('topic', '{{$rootUrl}}/mercure/demo/books/{id}');
    url.searchParams.append('topic', '{{$rootUrl}}/users/dunglas');

    const eventSource = new EventSource(url);

    // The callback will be called every time an update is published
    eventSource.onmessage = function (e) { // do something with the payload
      simplemde.value(simplemde.value()+"\n"+e.data);
      $('#lastEventId').text(e.lastEventId);
      console.log(e);
      console.log(moment(e.timeStamp).toDate());
    }
    var simplemde = new SimpleMDE({ element: $("#tristram")[0] });

  </script>
@stop
