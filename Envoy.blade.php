@servers(['web' => ['deployer@telemachus.bentleyworks.com']])
@setup
    if(!isset($user))
    {
        throw new Exception('User option not set.');
    }

    $server = $user . '@telemeachus.bentleysoft.com';
    $repo = 'git@gitlab.com:pdiveris/definitio2.git';
    $path = '/var/www/websites';

    if ( substr($path, 0, 1) !== '/' ) 
        throw new Exception('Careful - your deployment path does not begin with /');

    $date = ( new DateTime )->format('YmdHis');
    $env = isset($env) ? $env : "production";
 
    $branch = isset($branch) ? $branch : "master";
    $path = rtrim($path, '/');
    $release = $path.'/'.$date;
@endsetup

@task('deployment_start')
    echo "The path {{$path}}";
    cd {{ $path }};
    echo "Deployment ({{ $date }}) started";
    git clone {{ $repo }} --branch={{ $branch }} --depth=1 -q {{ $release }} ;
    echo "Repository cloned";
@endtask

@task('deployment_links')
    cd {{ $release }};
    chmod 777 bootstrap/*
    echo "Bootstrap permissions done";
    
    chmod 777 -R {{ $release }}/storage
    echo "Changed storage permission"
    
    cp {{ $release }}/.env.{{ $env }} {{ $release }}/.env
    echo "Change Environment link"
@endtask

@task('deployment_composer')
    cd {{ $release }};
    composer install --no-interaction --quiet;
@endtask

@task('deployment_npm')
    cd {{ $release }};
    npm install
@endtask

@task('deployment_migrate')
    php {{ $release }}/artisan migrate --env={{ $env }} --force --no-interaction;
@endtask

@task('deployment_queue')
    php {{ $release }}/artisan queue:restart --no-interaction;
@endtask

@task('deployment_cache')
    php {{ $release }}/artisan view:clear --quiet;
    php {{ $release }}/artisan cache:clear --quiet;
    php {{ $release }}/artisan config:cache --quiet;
    echo 'Cache cleared';
@endtask

@task('deployment_optimize')
    php {{ $release }}/artisan optimize --quiet;
@endtask

@task('deployment_finish')
    ln -nfs {{ $release }} {{ $path }}/current;
    echo "Deployment ({{ $date }}) finished";
@endtask

@task('deployment_cleanup')
    cd {{ $path }};
    find . -maxdepth 1 -name "20*" -mmin +2880 | head -n 5 | xargs rm -Rf;
    echo "Cleaned up old deployments";
@endtask

@story('deploy')
    deployment_start
    deployment_links
    deployment_composer
    deployment_migrate
    deployment_queue
    deployment_cache
    deployment_optimize
    deployment_finish
    deployment_cleanup
@endstory
