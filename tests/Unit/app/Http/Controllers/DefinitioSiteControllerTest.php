<?php
  
  namespace Tests\Feature;
  
  use App\Http\Controllers\DefinitioSiteController;
  use Tests\TestCase;
  use Illuminate\Foundation\Testing\RefreshDatabase;
  
  class DefinitioContentControllerTest extends TestCase
  {
    private $baseUrl;
    
    public function setUp() {
      parent::setUp();
      $this->baseUrl = env('APP_URL');
    }
  
    public function testAlphabetWithEntries() {
      $request = $this->getMockBuilder('Illuminate\Http\Request')
        ->disableOriginalConstructor()
        ->setMethods(['getMethod', 'retrieveItem', 'getRealMethod', 'all', 'getInputSource', 'get', 'has'])
        ->getMock();
    
      $sc = DefinitioSiteController::fascia($request);
    
      $response = $sc->alphabet($request);
    
      $blob = $response->render();
    
      $this->assertTrue($response->name()==='home');
      $this->assertRegexp('/<div id="kontainer"/', $blob);
    }
  
    /**
     * Test requesting gard existing term
     * 1. Look for terms
     * 2. If found pick one up
     * 3. Build a request
     * 4. GET
     * 5. Examine results..
     */
    public function testNavigateTerms()
    {
      $response = $this->call('GET', $this->baseUrl);
    
      $tagSoup = $response->getContent();
      $tags = substr($tagSoup, strpos($tagSoup, '<a class="button is-medium u-m-1'));
    
      $tags = substr($tags, 0, strrpos($tags, '<a class="button is-medium u-m-1')+150);
      $tags = str_replace('<a class="button is-medium u-m-1 is-inline-block-desktop"', '', $tags);
      $tags = trim($tags);
    
    
      $tags = explode("\n", $tags);
    
      $finalTags = [];
      foreach ($tags as $tag) {
        if('' !== trim($tag)) {
          $finalTags[] = $tag;
        }
      }
    
      $index = rand(0, count($finalTags));
    
      $tag = $finalTags[$index];
      $url = substr($tag, strpos($tag, "\"")+1);
      $title = substr($url, strpos($url, "\">")+2);
    
      $title = substr($title, 0,strpos($title, "<"));
      $url = substr($url, 0, strrpos($url, "\">"));
    
      $response = $this->call('GET', $url);
    
      $response->assertStatus(200);
    
      //<meta property="og:title" content="Apple genius"/>
      $this->assertRegexp('/<meta property="og:title" content="'.$title.'"\/>/', $response->getContent());
    }
  }
