<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 2019-02-11
   * Time: 21:47
   */
  namespace Tests\Feature;

  use App\Helpers\Utils;
  use Tests\TestCase;

  class RouteTest extends TestCase
  {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGuestRoutes()
    {
      $appURL = env('APP_URL');
    
      $urls = [
        '/term',
        '/alphabet',
        '/home',
        '/become',
        '/suggest',
        '/privacy',
        '/aboutus',
        '/about-us',
     
      ];
    
      echo  PHP_EOL;
    
      foreach ($urls as $url) {
        $response = $this->get($url);
        $msg = '';
        
        $action = Utils::getRouteControllerAction($url);
        
        if((int)$response->status() !== 200) {
          $msg = $appURL . $url . ' (FAILED) did not return a 200. ('.$response->status().') ';
          
          $this->assertTrue(false, $msg);
        } else {
          $msg = $appURL . $url . ' (http success)';
          $this->assertTrue(true, $msg);
        }

        if (is_array($action) && count($action) ==2 ) {
          $msg .= ' ';
          $msg .= $action['controller'] . '->' . $action['action'];
        }
        
        
        echo $msg;
        echo  PHP_EOL;
      }
    
    }
  }
  
