<?php
  /**
   * Created by PhpStorm.
   * User: pedro
   * Date: 2019-02-11
   * Time: 21:47
   */
  namespace Tests\Feature;

  use Tests\TestCase;

  class GuardedRoutesTest extends TestCase
  {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGuardedRoutes()
    {
      $appURL = env('APP_URL');
    
      $urls = [
        '/term',
      
      ];
    
      echo  PHP_EOL;
  
      foreach ($urls as $url) {
        $response = $this->get($url);
        if((int)$response->status() !== 200) {
          $msg = $appURL . $url . ' (FAILED) did not return a 200. ('.$response->status().')';
          $this->assertTrue(false, $msg);
        } else {
          $msg = $appURL . $url . ' (http success)';
          $this->assertTrue(true, $msg);
        }
        echo $msg;
        echo  PHP_EOL;
      }
    
    }
  }
  
