<?php
  
  namespace Tests\Feature;
  
  use Tests\TestCase;
  
  class ExampleTest extends TestCase
  {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
      $response = $this->call('GET', 'https://www.definitio.org/alphabet?q=b');
      
      $response->assertStatus(200);
      $this->assertRegexp('/<title>definitio/', $response->getContent());
    }

  }
