<?php
/**
 * FInderDirectoryElement maps a directory entry
 *
 * Handles aspects of the login process,
 * including syncing with OAuth data from external providers
 *
 * PHP version 7.2
 *
 * LICENSE: This source file is subject to version 2.0 of the Apache License
 * that is available through the world-wide-web at the following URI:
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * @category
 * @package
 * @author    Petros Diveris <petros@diveris.org>
 * @copyright 2019 Bentleyworks
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   GIT:
 * @link      https://github.com/pdiveris/sixproposals/blob/master/app/Http/Controllers/Auth/LoginController.php
 * @see       Six Acts
 */

namespace Bentleysoft\Finder;

class FinderDirectoryElement extends FinderElement
{
    /**
     * @return mixed|string
     */
    public function __toString()
    {
        $array = [
            'name'=>'PANGA'
        ];
        $ret = json_encode($array);
        return (string)$ret;
    }
    
    /**
     * @return string
     */
    public function jsonSerialize(): string
    {
        $array = [
            'name'=>$this->getName(),
            'path'=>$this->getPath(),
            'size'=>$this->getSize(),
            'type'=>$this->getType(),
            'icon'=>$this->getIcon(),
            'attributes'=>$this->getAttributes(),
            'disk'=>$this->getDisk()
        ];
        return json_encode($array);
    }

}
