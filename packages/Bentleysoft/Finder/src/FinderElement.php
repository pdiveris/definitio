<?php
/**
 * Finder base Element
 *
 * Handles aspects of the login process,
 * including syncing with OAuth data from external providers
 *
 * PHP version 7.2
 *
 * LICENSE: This source file is subject to version 2.0 of the Apache License
 * that is available through the world-wide-web at the following URI:
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * @category
 * @package
 * @author    Petros Diveris <petros@diveris.org>
 * @copyright 2019 Bentleyworks
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   GIT:
 * @link      https://github.com/pdiveris/sixproposals/blob/master/app/Http/Controllers/Auth/LoginController.php
 * @see       Six Acts
 */

namespace Bentleysoft\Finder;

use Bentleysoft\FileNavigator\FileUtilities;

/**
 * Class FinderElement
 * @package Bentleysoft\Finder
 */
abstract class FinderElement implements \JsonSerializable
{
    protected $disk = 'public';
    
    /**
     * @var string
     */
    protected $name = '';
    
    /**
     * @var string
     */
    protected $path = '';
    
    /**
     * @var string
     */
    protected $extension = '';
    /**
     * @var string
     */
    protected $type = '';
    
    /**
     * @var string
     */
    protected $mimeType = '';
    
    /**
     * @var int
     */
    protected $size = 0;
    
    /**
     * @var string
     */
    protected $timeStamp = '';
    
    /**
     * @var string
     */
    protected $attributes = '';
    
    /**
     * @var string
     */
    protected $icon = '';
    
    /**
     * @var string
     */
    protected $preview = '';
    
    /**
     * @var string
     */
    protected $etag = '';
    
    /**
     * @var string
     */
    protected $url = '';
    
    /**
     * @var string
     */
    protected $previewUrl = '';
    
    /**
     * @var array
     */
    protected $children = [];
    
    /**
     * FinderElement constructor.
     *
     * @param $name
     * @param $path
     * @param string $disk
     */
    public function __construct($name, $path, $disk = '')
    {
        $this->name = $name;
        $this->path = $path;

        if (! ($disk === '')) {
            $this->disk = $disk;
        }
    }
    
    /**
     * Get a string representation of it.
     *
     * @return mixed
     */
    abstract public function __toString();
    
    /**
     * Get a string representation of it.
     *
     * @return string
     */
    abstract public function jsonSerialize(): string;
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    
    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
    
    /**
     * @return string
     * @throws \Exception
     */
    public function getExtension(): string
    {
        throw new \Exception('Extension not supported by this object', 5541);
    }
    
    /**
     * @param string $extension
     * @throws \Exception
     */
    public function setExtension(string $extension): void
    {
        throw new \Exception('Extension not supported by this object', 5541);
    }
    
    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }
    
    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }
    
    /**
     * @return string
     * @throws \Exception
     */
    public function getUrl(): string
    {
        throw new \Exception('Attribute URL not supported by object', 5542);
    }
    
    /**
     * @param string $url
     * @throws \Exception
     */
    public function setUrl(string $url): void
    {
        throw new \Exception('Attribute URL not supported by object', 5542);
    }
    
    /**
     * @return string
     * @throws \Exception
     */
    public function getPreviewUrl(): string
    {
        throw new \Exception('Attribute URL not supported by object', 5542);
    }
    
    /**
     * @param string $previewUrl
     * @throws \Exception
     */
    public function setPreviewUrl(string $previewUrl): void
    {
        throw new \Exception('Attribute URL not supported by object', 5542);
    }
    
    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }
    
    /**
     * @return string
     */
    public function getEtag(): string
    {
        return $this->etag;
    }
    
    /**
     * @param $etag
     */
    public function setEtag($etag): void
    {
        $this->etag = $etag;
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @param $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }
    
    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }
    
    /**
     * @param $mimeType
     */
    public function setMimeType($mimeType): void
    {
        $this->mimeType = $mimeType;
    }
    
    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }
    
    /**
     * @param $size
     */
    public function setSize($size): void
    {
        $this->size = $size;
    }
    
    /**
     * @return string
     */
    public function getTimeStamp(): string
    {
        return $this->timeStamp;
    }
    
    /**
     * @param $timeStamp
     */
    public function setTimeStamp($timeStamp): void
    {
        $this->timeStamp = $timeStamp;
    }
    
    /**
     * @param string $fmt
     * @return string
     */
    public function getLocalTimeStamp($fmt = 'Y-m-d H:i:s')
    {
        return \DateTime::createFromFormat('U', 1570704407)
            ->format($fmt);
    }
    
    /**
     * @return string
     */
    public function getAttributes(): string
    {
        return $this->attributes;
    }
    
    /**
     * @param $attributes
     */
    public function setAttributes($attributes): void
    {
        $this->attributes = $attributes;
    }
    
    /**
     * Get an icon
     *
     * @link https://princenekretnine.com/css/fontawesome-free-5.0.6/advanced-options/raw-svg/regular/file-image.svg
     * @return string
     */
    public function getIcon(): string
    {
        if ($this->icon !== '') {
            return $this->icon;
        }
        if ($this->type === 'dir') {
            return asset('/assets/icons/dir.svg');
        }
        if (file_exists(public_path('/assets/icons/'.$this->extension.'.svg'))) {
            return asset('/assets/icons/' . $this->extension . '.svg');
        }
        return asset('/assets/icons/file.svg');
    }
    
    /**
     * @param $icon
     */
    public function setIcon($icon): void
    {
        $this->icon = $icon;
    }
    
    /**
     * @return string
     * @throws \Exception
     */
    public function getPreview(): string
    {
        return $this->preview;
    }
    
    /**
     * @param string $preview
     */
    public function setPreview(string $preview): void
    {
        $this->preview = $preview;
    }
    
    /**
     * @return string
     */
    public function getDisk(): string
    {
        return $this->disk;
    }
    
    /**
     * @param string $disk
     */
    public function setDisk(string $disk): void
    {
        $this->disk = $disk;
    }
    
    
    /**
     * @param array $pathInfo
     * @param string $disk
     * @return \Bentleysoft\Finder\FinderDirectoryElement|\Bentleysoft\Finder\FinderFileElement|null
     */
    public static function createFromPath(array $pathInfo, string $disk)
    {
        $ret = null;
        if ($pathInfo['type'] === 'dir') {
            $ret = new FinderDirectoryElement(
                $pathInfo['basename'],
                $pathInfo['dirname'],
                $disk
            );
            $ret->setType('dir');
        }
    
        if ($pathInfo['type'] === 'file') {
            $ret = new FinderFileElement(
                $pathInfo['basename'],
                $pathInfo['dirname'],
                $disk
            );
            $ret->setSize($pathInfo['size']);
            $ret->setEtag($pathInfo['etag']);
            $ret->setExtension($pathInfo['extension']);
            $ret->setTimeStamp($pathInfo['timestamp']);
            $ret->setMimeType(FileUtilities::bruteGuessMime($pathInfo['basename']));
            $ret->setType('file');
        }
        return $ret;
    }
}
