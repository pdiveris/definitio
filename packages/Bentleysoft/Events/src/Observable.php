<?php
/**
 * Observable
 *
 * Implements funcitonality allowint an object to be 'observed'
 *
 * PHP version 7.2
 *
 * LICENSE: This source file is subject to version 2.0 of the Apache License
 * that is available through the world-wide-web at the following URI:
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * @category
 * @package
 * @author    Petros Diveris <petros@diveris.org>
 * @copyright 2019 Bentleyworks
 * @license   https://www.apache.org/licenses/LICENSE-2.0.txt Apache-2.0
 * @version   GIT:
 * @link      https://stackoverflow.com/questions/13774149/how-is-splsubject-splobserver-useful
 * @link      https://codeburst.io/observer-pattern-object-oriented-php-4e669431bcb9
 * @see       \Bentleysoft\Events\Observable
 */

namespace Bentleysoft\Events;

use SplObserver;
use SplSubject;

/**
 * Class Observable
 *
 * @package Bentleysoft\Events
 */
abstract class Observable implements SplSubject
{
    /**
     * @var array
     */
    protected $observers = array();
    
    /**
     * @var array
     */
    protected $linkedList = array();
    
    /**
     *
     * @param string
     */
    protected $name;
    
    /**
     *
     * @param string
     */
    protected $event;
    
    /**
     *  Associate an observer
     *
     * @param SplObserver $observer
     */
    public function attach(SplObserver $observer)
    {
        $observerKey = spl_object_hash($observer);
        $this->observers[$observerKey] = $observer;
        $this->linkedList[$observerKey] = $observer->getPriority();
        arsort($this->linkedList);
    }
    
    /**
     * @param SplObserver $observer
     * @return void
     */
    public function detach(SplObserver $observer)
    {
        $observerKey = spl_object_hash($observer);
        unset($this->observers[$observerKey], $this->linkedList[$observerKey]);
    }
    
    /**
     * @return void
     */
    public function notify()
    {
        foreach ($this->linkedList as $key => $value) {
            $this->observers[$key]->update($this);
        }
    }
    
    /**
     * Set or update event
     *
     * @param  $event
     * @return void
     */
    public function setEvent($event)
    {
        $this->event = $event;
        $this->notify();
    }
    
    /**
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }
    
    public function getSubscribers()
    {
        return $this->getSubscribers();
    }
}
