<?php
/**
 * Observer
 *
 * Observes changes in registered components and fires events
 *
 * PHP version 7.2
 *
 * LICENSE: This source file is subject to version 2.0 of the Apache License
 * that is available through the world-wide-web at the following URI:
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * @category
 * @package
 * @author    Petros Diveris <petros@diveris.org>
 * @copyright 2019 Bentleyworks
 * @license   https://www.apache.org/licenses/LICENSE-2.0.txt Apache-2.0
 * @version   GIT:
 * @link      https://codeburst.io/observer-pattern-object-oriented-php-4e669431bcb9
 * @link      https://stackoverflow.com/questions/13774149/how-is-splsubject-splobserver-useful
 * @see       Observable
 */

namespace Bentleysoft\Events;

use SplObserver;
use SplSubject;

/**
 * Class Observer
 *
 * @package Bentleysoft\Events
 * @see     https://codeburst.io/observer-pattern-object-oriented-php-4e669431bcb9
 */
class Observer implements SplObserver
{
    /**
     * @var string
     */
    protected $name;
    
    /**
     * @var int
     */
    protected $priority = 0;
    
    /**
     * Accepts observer name and priority, default to zero
     */
    public function __construct($name, $priority = 0)
    {
        $this->name = $name;
        $this->priority = $priority;
    }
    
    /**
     * Get observer priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
    
    /**
     * Receive update from subject and print result
     * So I guess in here we can call something that needs doing,
     * e.g. calculating subdirectories etc..
     *
     * @param SplSubject $publisher
     * @return void
     */
    public function update(SplSubject $publisher)
    {
        // dump($this->name.': '. $publisher->getEvent().'->'.$publisher->getName());
    }
}
