<?php
/**
 * FileSystemElement
 *
 * Handles aspects of the login process,
 * including syncing with OAuth data from external providers
 *
 * PHP version 7.2
 *
 * LICENSE: This source file is subject to version 2.0 of the Apache License
 * that is available through the world-wide-web at the following URI:
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * @category  System
 * @package   Filesystem
 * @author    Petros Diveris <petros@diveris.org>
 * @copyright 2019 Bentleyworks
 * @license   https://www.apache.org/licenses/LICENSE-2.0.txt Apache-2.0
 * @version   GIT:
 * @link      https://github.com/pdiveris/sixproposals/blob/master/app/Http/Controllers/Auth/LoginController.php
 * @see       definitio3
 */

namespace Bentleysoft\FileNavigator;

use Bentleysoft\Events\Observable;
use Exception;

/**
 * Class FileSystemElement
 *
 * @package Bentleysoft\Filesystem
 *
 * @category
 * @author
 * @license
 * @link
 */
abstract class FileSystemResource extends Observable
{
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @var array
     */
    public static $fsTypes = ['dir', 'file'];
    
    /**
     * FileSystemElement constructor.
     *
     * @param string|null $name
     */
    public function __construct(string $name = '')
    {
        $this->name = $name;
    }
    
    /**
     * @return string
     */
    function __toString()
    {
        return "FileSystemResource";
    }
    
    /**
     * @param \Bentleysoft\FileNavigator\FileSystemResource $resource
     * @throws \Exception
     */
    public function addResource(FileSystemResource $resource): void
    {
        throw new Exception("Invalid operation", 5520);
    }
    
    /**
     * @throws \Exception
     */
    public function getMimeType()
    {
        throw new Exception("Invalid operation", 5523);
    }
    
    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param int $index
     * @return \Bentleysoft\FileNavigator\FileSystemResource
     * @throws \Exception
     */
    public function getResource(int $index)
    {
        throw new Exception("Invalid operation", 5522);
    }
    
    /**
     * @param string $name
     * @return \Bentleysoft\FileNavigator\FileSystemResource
     * @throws \Exception
     */
    public function getResourceByName(string $name)
    {
        throw new Exception("Invalid operation", 5522);
    }
    
    /**
     * @return string
     */
    abstract public function getType(): string;
    
    /**
     * @return int
     */
    abstract public function getSize(): int;
    
    /**
     * @param \Bentleysoft\FileNavigator\FileSystemResource $resource
     * @return bool
     * @throws \Exception
     */
    public function removeResource(FileSystemResource $resource): bool
    {
        throw new Exception("Invalid operation", 5521);
    }
}
