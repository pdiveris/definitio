<?php
/**
 * DirectoryResource to represent a folder
 *
 * Handles aspects of the login process,
 * including syncing with OAuth data from external providers
 *
 * PHP version 7.2
 *
 * LICENSE: This source file is subject to version 2.0 of the Apache License
 * that is available through the world-wide-web at the following URI:
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * @category
 * @package
 * @author    Petros Diveris <petros@diveris.org>
 * @copyright 2019 Bentleyworks
 * @license   https://www.apache.org/licenses/LICENSE-2.0.txt Apache-2.0
 * @version   GIT:
 * @link      https://github.com/pdiveris/sixproposals/blob/master/app/Http/Controllers/Auth/LoginController.php
 * @see       Six Acts
 */

namespace Bentleysoft\FileNavigator;

use Bentleysoft\Events\Observer;
use Exception;
use function array_splice;

/**
 * Class DirectoryResource
 *
 * @package Bentleysoft\FileNavigator
 */
class DirectoryResource extends FileSystemResource
{
    /**
     * @var array
     */
    protected $resources = [];
    
    /**
     * DirectoryResource constructor.
     *
     * @param string $name
     */
    public function __construct($name = '')
    {
        parent::__construct($name);
    }
    
    /**
     * @param \Bentleysoft\FileNavigator\FileSystemResource $resource
     * @throws \Exception
     */
    public function addResource(FileSystemResource $resource): void
    {
        $this->resources[] = $resource;
        
        if ($resource->getType() === ' file') {
            $observer = new Observer('FileObserver', 50);
        } else {
            $observer = new Observer('DirectoryObserver', 30);
        }
        
        $resource->attach($observer);
        $resource->setEvent(sprintf("Contents of resource read (%s)", $resource->getType()));
    }
    
    /**
     * @param int $index
     * @return \Bentleysoft\FileNavigator\FileSystemResource
     * @throws \Exception
     */
    public function getResource(int $index)
    {
        if ($index <= count($this->resources)) {
            return $this->resources[$index];
        }
        throw new Exception("Invalid resource", 5522);
    }
    
    /**
     * @param string $name
     * @return \Bentleysoft\FileNavigator\FileSystemResource
     * @throws \Exception
     */
    public function getResourceByName(string $name)
    {
        foreach ($this->resources as $resource) {
            if ($resource->getName() === $name) {
                return $resource;
            }
        }
        return null;
    }
    
    /**
     * @param array $pathInfo
     * @return \Bentleysoft\FileNavigator\DirectoryResource|\Bentleysoft\FileNavigator\FileResource
     * @throws \Exception
     */
    
    /**
     * Get size of directory (sum of all childrens' sizes)
     *
     * @return int
     */
    public function getSize(): int
    {
        $size = 0;
        foreach ($this->resources as $res) {
            $size += $res->getSize();
        }
        return $size;
    }
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'directory';
    }
    
    /**
     * @param \Bentleysoft\FileNavigator\FileSystemResource $resource
     * @return bool
     * @throws \Exception
     */
    public function removeResource(FileSystemResource $resource): bool
    {
        $i = 0;
        foreach ($this->resources as $res) {
            if ($res === $resource) {
                array_splice($this->resources, $i, 1);
                return true;
            }
            $i++;
        }
        return false;
    }
    
    /**
     * @return array
     */
    public function getResources()
    {
        return $this->resources;
    }
}
