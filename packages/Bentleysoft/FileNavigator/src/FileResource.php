<?php
/**
 * File resource
 *
 * Handles aspects of the login process,
 * including syncing with OAuth data from external providers
 *
 * PHP version 7.2
 *
 * LICENSE: This source file is subject to version 2.0 of the Apache License
 * that is available through the world-wide-web at the following URI:
 * https://www.apache.org/licenses/LICENSE-2.0.
 *
 * @category  System
 * @package   Filesystem
 * @author    Petros Diveris <petros@diveris.org>
 * @copyright 2019 Bentleyworks
 * @license   https://www.apache.org/licenses/LICENSE-2.0.txt Apache-2.0
 * @version   GIT:
 * @link      https://github.com/pdiveris/sixproposals/blob/master/app/Http/Controllers/Auth/LoginController.php
 * @see       Six Acts
 */

namespace Bentleysoft\FileNavigator;

use Bentleysoft\Events\Observer;

class FileResource extends FileSystemResource
{
    /**
     * @var int|null
     */
    protected $size = 0;
    
    /**
     * @var string
     */
    protected $mime = 'undefined';
    
    /**
     * FileSystemElement constructor.
     *
     * @param string|null $name
     * @param int|null $size
     * @param string|null $mime
     */
    public function __construct(string $name = '', int $size = 0, $mime = '')
    {
        parent::__construct($name);
        
        $this->size = $size;
        $this->mime = $mime;
        
        if ($this->mime === 'undefined' || $this->mime === '') {
            $this->mime = FileUtilities::bruteGuessMime($name);
        }
        // $this->notify();
    }
    
    /**
     * @throws \Exception
     */
    public function getMimeType()
    {
        return $this->mime;
    }
    
    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'file';
    }
}
