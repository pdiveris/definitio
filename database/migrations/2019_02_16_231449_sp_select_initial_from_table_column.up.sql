-- Run the migrations
DROP PROCEDURE IF EXISTS fn_firstletter;
CREATE PROCEDURE `fn_firstletter`(IN col VARCHAR(120), IN tbl VARCHAR(120))
BEGIN
    SET @sql = CONCAT('SELECT LCASE(SUBSTRING(', col, ', 1, 1)) AS first FROM `',tbl,'` GROUP BY `first` ORDER BY `first`');
    PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END
