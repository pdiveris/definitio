-- Run the migrations
DROP view IF EXISTS items;
CREATE VIEW `items`
AS SELECT
       `terms`.`id` AS `id`,
       `terms`.`expression` AS `expression`,
        fn_urltitle(`terms`.`expression`) AS `slug`,
       `terms`.`content_html` AS `content_html`,
       `terms`.`content_markdown` AS `content_markdown`,
       `terms`.`definitio` AS `definitio`,
       `terms`.`src` AS `src`,
       `terms`.`user_id` AS `user_id`,
       `terms`.`time_point` AS `time_point`,
       `terms`.`deleted_at` AS `deleted_at`,
       `terms`.`created_at` AS `created_at`,
       `terms`.`updated_at` AS `updated_at`,
       `terms`.`published_at` AS `published_at`
   FROM `terms`;
