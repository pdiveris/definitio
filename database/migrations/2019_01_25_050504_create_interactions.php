<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteractions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    Schema::create('interactions', function (Blueprint $table) {
      $table->increments('id');
  
      // e.g. 'view'
      $table->string('request_type', 255);
      // e.g. 'facebook'
      $table->string('referrer', 400);
      $table->string('ref_key', 255);
      
      $table->string('ip_address', 255)->nullable();
      $table->mediumText('meta', '')->nullable();
      $table->mediumText('header', '')->nullable();
  
      $table->timestamps();
  
  
    });
  }
  
  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('interactions');
  }

}
