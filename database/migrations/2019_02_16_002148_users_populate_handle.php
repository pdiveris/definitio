<?php
  
  use Illuminate\Support\Facades\Schema;
  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;
  
  class UsersPopulateHandle extends Migration
  {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //
  
      $users = \App\User::all();
      foreach ($users as $i => $user) {
        echo 'Doing ';
        echo $user->id;
        echo ' ';
        echo $user->name;
        echo "...\n";
    
        $user->handle =
            strtolower(substr($user->name, 0, 1))
          . strtolower(substr($user->name, strpos($user->name, ' ')+1));
    
        $user->save();
      }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //
      $users = \App\User::all();
      foreach ($users as $i => $user) {
        echo 'Undoing ';
        echo $user->id;
        echo ' ';
        echo $user->name;
        echo "...\n";
        $user->handle = '';
    
        $user->save();
      }
    }
  }
