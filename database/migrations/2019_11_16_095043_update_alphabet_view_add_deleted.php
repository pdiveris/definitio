<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAlphabetViewAddDeleted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<EOT

CREATE OR REPLACE
VIEW `definitio`.`alphabet` AS
select
    lcase(substr(`items`.`expression`,
    1,
    1)) AS `initial`,
    `items`.`deleted_at`
from
    `definitio`.`items`
group by
    lcase(substr(`items`.`expression`,
    1,
    1)),
`items`.`deleted_at`
order by
    lcase(substr(`items`.`expression`,
    1,
    1))
EOT;
    
        DB::unprepared($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
