<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('links');
        
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('label', 400)
                ->default('')
                ->index();
            
            $table->string('uri', 2000)
                ->default('');
    
            $table->integer('user_id')->unsigned();
            $table->integer('term_id')->unsigned();
            
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            
            $table->foreign('term_id')
                ->references('id')
                ->on('terms');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
