<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Markdownify\ConverterExtra;

class TermsPopulateMdFromHtml extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $converter = new ConverterExtra();
      
      $terms = \App\Term::all();
      foreach ($terms as $i => $term) {
        echo 'Doing ';
        echo $term->id;
        echo ' ';
        echo $term->expression;
        echo '...\n';
        $term->content_markdown = $converter->parseString($term->classic);
        $term->save();
      }
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      // set term->content_* to ''
      $terms = \App\Term::all();
      foreach ($terms as $i => $term) {
        echo '\nDOWN!  \n';
        echo 'Resetting ';
        echo $term->id;
        echo ' ';
        echo $term->expression;
        echo "...\n";
        $term->content_markdown = '';
    
        $term->save();
      }
    }
}
