-- Run the migrations
DROP VIEW IF EXISTS `related`;
CREATE VIEW `related`
AS SELECT
       `items`.`id` AS `id`,
       `items`.`expression` AS `expression`,
       `definitio`.`items`.`definitio` AS `definitio`,
       `definitio`.`items`.`src` AS `src`,
       `definitio`.`items`.`user_id` AS `user_id`,
       `definitio`.`items`.`time_point` AS `time_point`,
       `definitio`.`items`.`deleted_at` AS `deleted_at`,
       `definitio`.`items`.`created_at` AS `created_at`,
       `definitio`.`items`.`updated_at` AS `updated_at`
   FROM `items`;
