<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TermsAddContentFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('terms', function (Blueprint $table) {
        $table->mediumText('content_html')->default('')->after('classic');
        $table->mediumText('content_markdown')->default('')->after('content_html');
      });
    }
  
  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('terms', function (Blueprint $table) {
      $table->removeColumn('content_html');
      $table->removeColumn('content_markdown');
    });
  }
}
