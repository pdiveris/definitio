<?php
  
  use Illuminate\Support\Facades\Schema;
  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;
  
  class DefinitionsAddContentFields extends Migration
  {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('definitions', function (Blueprint $table) {
        $table->mediumText('content_html')
          ->nullable()
          ->default('')
          ->after('body');
        
        $table->mediumText('content_markdown')
          ->nullable()
          ->default('')
          ->after('content_html');
      });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //
      Schema::table('definitions', function (Blueprint $table) {
        $table->removeColumn('content_html');
        $table->removeColumn('content_markdown');
      });
      
    }
  }
