CREATE FUNCTION `fn_urltitle`( `title` varchar(460) )
RETURNS varchar(460) CHARSET utf8
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE url_title varchar(460) CHARACTER SET utf8;
    SET url_title = replace(title,'<', '');
    SET url_title = replace(url_title,'>', '');
    SET url_title = replace(url_title,'#', '');
    SET url_title = replace(url_title,'%', '');
    SET url_title = replace(url_title,'{', '(');
    SET url_title = replace(url_title,'}', ')');
    SET url_title = replace(url_title,'|', '-');
    SET url_title = replace(url_title,'[', '(');
    SET url_title = replace(url_title,']', ')');
    SET url_title = replace(url_title,'`', '');
    SET url_title = replace(url_title,';', '');
    SET url_title = replace(url_title,'?', '');
    SET url_title = replace(url_title,':', '');
    SET url_title = replace(url_title,'@', '');
    SET url_title = replace(url_title,'&', '-and-');
    SET url_title = replace(url_title,'.', '');
    SET url_title = replace(url_title,'=', '');
    SET url_title = replace(url_title,',', '-');
    SET url_title = replace(url_title,'+', '');
    SET url_title = replace(url_title,'$', 'USD');
    SET url_title = replace(url_title,'£', 'GBP');
    SET url_title = replace(url_title,'?', 'EUR');
    SET url_title = replace(url_title,'\\', '-');
    SET url_title = replace(url_title,'\/', '-');
    SET url_title = replace(url_title,'^', '');
    SET url_title = replace(url_title,' ', '-');
    SET url_title = replace(url_title,'--', '-');
    RETURN lcase(url_title);
END
