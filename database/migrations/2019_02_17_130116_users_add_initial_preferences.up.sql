-- Run the migrations
UPDATE users
SET preferences =
        JSON_OBJECT(
            "editor",
            JSON_ARRAY("markdown","html"),
            "theme",
            "kosher",
            "spelling_checker",
            FALSE
            )
where id=1;

UPDATE users
SET preferences =
        JSON_OBJECT(
            "editor",
            JSON_ARRAY("html","markdown"),
            "theme",
            "kosher",
            "spelling_checker",
            FALSE
            )
where id>2;

