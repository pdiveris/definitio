-- Run the migrations
DROP view IF EXISTS editors;
create view editors as
select id, name, email, handle
from users
where enabled=1

