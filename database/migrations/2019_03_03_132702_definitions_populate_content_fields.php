<?php
  
  use Illuminate\Support\Facades\Schema;
  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;
  use App\Http\Controllers\DefinitioSiteController;
  
  class DefinitionsPopulateContentFields extends Migration
  {
    /**
     * Run the migrations.
     *
     * @return void
     * @throws \Exception
     */
    public function up()
    {
      $definitions = \App\Definition::all();
      foreach ($definitions as $i => $def) {
        echo 'Doing ';
        echo $def->id;
        echo ' ';
        echo $def->heading;
        echo "...\n";

        $def->content_markdown = $def->body;
        if (!null==$def->body) {
          echo "Converting MD to HTML\n";
          $def->content_html = DefinitioSiteController::parseDom($def->body);
        } else {
          echo "NOT Converting MD to HTML\n";
        }
        $def->save();
      }
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      $definitions = \App\Definition::all();
      foreach ($definitions as $i => $def) {
        echo "Undoing ";
        
        echo $def->id;
        echo ' ';
        echo $def->heading;
        echo '...\n';
        $def->content_html = '';
        $def->content_markdown = '';
        $def->save();
      }
    }
  }
