-- Run the migrations
CREATE
    OR REPLACE
    VIEW `definitio`.`editors` AS select
                                      `definitio`.`users`.`id` AS `id`,
                                      `definitio`.`users`.`name` AS `name`,
                                      `definitio`.`users`.`email` AS `email`,
                                      `definitio`.`users`.`preferences` AS `preferences`,
                                      `definitio`.`users`.`handle` AS `handle`
                                  from
                                      `definitio`.`users`
                                  where
                                          `definitio`.`users`.`enabled` = 1
