<?php

use Illuminate\Database\Migrations\Migration;

class TermsPopulateHtmlFromClassic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $terms = \App\Term::all();
      foreach ($terms as $i => $term) {
        echo 'Doing ';
        echo $term->id;
        echo ' ';
        echo $term->expression;
        echo '...\n';
        $term->content_html = $term->classic;
        $term->save();
      }
    }

    /**
     * Reverse the migrations.

     *
     * @return void
     */
    public function down()
    {
      // set term->content_* to ''
      $terms = \App\Term::all();
      foreach ($terms as $i => $term) {
        echo '\nDOWN!  \n';
        echo 'Resetting ';
        echo $term->id;
        echo ' ';
        echo $term->expression;
        echo "...\n";
        $term->content_html = '';
    
        $term->save();
      }
    }
}
