<?php
  
  use Illuminate\Support\Facades\Schema;
  use Illuminate\Database\Schema\Blueprint;
  use Illuminate\Database\Migrations\Migration;
  
  class TermsRemoveColumnClassic extends Migration
  {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      //
      Schema::table('terms', function (Blueprint $table) {
        $table->removeColumn('classic');
      });
      
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      //
      Schema::table('terms', function (Blueprint $table) {
        $table->mediumText('classic')->nullable()->default('');
      });
    }
  }
