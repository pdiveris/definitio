<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTermsAddNoHomeLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('terms', function (Blueprint $table) {
            $table->boolean('no_home_link')
                ->after('user_id')
                ->default(false)
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('terms', 'no_home_link')) {
            Schema::table('terms', function (Blueprint $table) {
                $table->dropColumn('no_home_link');
            });
        }
    }
}
