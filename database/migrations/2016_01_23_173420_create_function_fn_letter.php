<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionFnLetter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<EOT
CREATE DEFINER=`root`@`localhost` PROCEDURE `definitio`.`fn_firstletter`(IN col VARCHAR(120), IN tbl VARCHAR(120))
BEGIN
    SET @sql = CONCAT('SELECT LCASE(SUBSTRING(', col, ', 1, 1)) AS first FROM `',tbl,'` GROUP BY `first` ORDER BY `first`');
    PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END;
EOT;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
