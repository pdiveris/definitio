delimiter //
DROP TRIGGER IF EXISTS tags_timestamps_update;
CREATE TRIGGER tags_timestamps_update BEFORE UPDATE ON tags
    FOR EACH ROW
BEGIN
    IF NEW.created_at IS NULL THEN
        SET NEW.created_at = NOW();
    END IF;
    IF NEW.updated_at IS NULL THEN
        SET NEW.updated_at = NOW();
    END IF;
END;//
delimiter ;
