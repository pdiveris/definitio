delimiter //
DROP TRIGGER IF EXISTS links_timestamps_insert;
CREATE TRIGGER links_timestamps_insert BEFORE INSERT ON links
    FOR EACH ROW
BEGIN
    IF NEW.created_at IS NULL THEN
        SET NEW.created_at = NOW();
    END IF;
    IF NEW.updated_at IS NULL THEN
        SET NEW.updated_at = NOW();
    END IF;
END;//
delimiter ;
