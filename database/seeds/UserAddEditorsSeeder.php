<?php
  
  use Illuminate\Database\Seeder;
  
  class UserAddEditorsSeeder extends Seeder
  {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //
      $role_editor  = Role::where('name', 'editor')->first();
  
      $manager = new User();
      $manager->name = 'Ewa Jasiewicz';
      $manager->email = 'freelance@mailworks.org';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Eleanor Bullen';
      $manager->email = 'efbullen@yahoo.com';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Lloyd Edwards';
      $manager->email = 'lledwards80@googlemail.com';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Michael Menegos';
      $manager->email = 'michaelmenegos@yahoo.com';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Ursula Rothe';
      $manager->email = 'usrula.rothe@gmx.net';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Joe Shaw';
      $manager->email = 'joeshaw@posteo.net';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Asa Calow';
      $manager->email = 'asa@madlab.org.uk';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Rachel Robertson';
      $manager->email = 'rachelrobbo@hotmail.com';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Rachael Turner';
      $manager->email = 'rachaelturner1@gmail.com';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
      $manager = new User();
      $manager->name = 'Red Paperer ';
      $manager->email = 'ted@definitio.org';
      
      $manager->enabled = 1;
      $manager->save();
  
      $manager->roles()->attach($role_editor);
      
    }
  }
